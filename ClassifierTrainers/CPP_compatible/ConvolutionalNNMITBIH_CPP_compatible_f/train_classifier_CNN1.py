import numpy as np
import pandas as pd
import random
import matplotlib.pyplot as plt
import matplotlib.tri as mtri

def add_gaussian_noise(signal):
    noise=np.random.normal(0,0.05,186)
    return (signal+noise)

mitbih = pd.read_csv("../../dataset/mitbih_test.csv", header = None)
mitbih_train = pd.read_csv("../../dataset/mitbih_train.csv", header = None)

data = mitbih_train.iloc[:, :186].values
labels = mitbih_train.iloc[:, 187].values

from keras.utils import to_categorical

one_hot_labels = np.zeros((len(labels), 5))
one_hot_labels[:] = to_categorical(labels[:], num_classes = 5)

from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(data, one_hot_labels, test_size = 0.33, random_state = 42)

from keras.models import Sequential
from keras.layers import Input, Dense, Activation, BatchNormalization, Dropout, Convolution1D, MaxPooling1D, SeparableConv1D, Flatten, MaxPool1D
from keras.optimizers import Adam
from keras.models import Model

########################
print('############################################\n')
print("Pocinje:\n")
print('############################################\n')

X_train[:] = add_gaussian_noise(X_train[:])#Small data augmentation
X_test[:] = add_gaussian_noise(X_test[:])#Small data augmentation

X_train = X_train.reshape(len(X_train), X_train.shape[1],1)
X_test = X_test.reshape(len(X_test), X_test.shape[1],1)

im_shape=(X_train.shape[1],1)
inputs_cnn=Input(shape=(im_shape), name='inputs_cnn')
conv1_1=Convolution1D(64, (6), activation='relu', input_shape=im_shape)(inputs_cnn)
conv1_1=BatchNormalization()(conv1_1)
pool1=MaxPool1D(pool_size=(3), strides=(2), padding="same")(conv1_1)
conv2_1=Convolution1D(64, (3), activation='relu', input_shape=im_shape)(pool1)
conv2_1=BatchNormalization()(conv2_1)
pool2=MaxPool1D(pool_size=(2), strides=(2), padding="same")(conv2_1)
conv3_1=Convolution1D(64, (3), activation='relu', input_shape=im_shape)(pool2)
conv3_1=BatchNormalization()(conv3_1)
pool3=MaxPool1D(pool_size=(2), strides=(2), padding="same")(conv3_1)
flatten=Flatten()(pool3)
dense_end1 = Dense(64, activation='relu')(flatten)
dense_end2 = Dense(32, activation='relu')(dense_end1)
main_output = Dense(5, activation='softmax', name='main_output')(dense_end2)


model = Model(inputs= inputs_cnn, outputs=main_output)
########################

model.compile(optimizer = Adam(lr = 100e-5), loss = "categorical_crossentropy", metrics = ["accuracy"])

EPOCHS = 5
history = model.fit(X_train, y_train, validation_split = 0.2, epochs = EPOCHS, shuffle = True, class_weight = 'auto')

print(model.evaluate(X_test, y_test))

#N = np.arange(0, EPOCHS)
#title = "Training loss and Acuracy"

#plt.style.use("ggplot")
#lt.figure()
#plt.plot(N, history.history["loss"], label = "train_loss")
#plt.plot(N, history.history["val_loss"], label = "val_loss")
#plt.plot(N, history.history["accuracy"], label = "train_acc")
#plt.plot(N, history.history["val_accuracy"], label = "val_acc")
#plt.title(title)
#plt.xlabel("Epoch #")
#plt.ylabel("Loss/Accuracy")
#plt.legend()


#from keras.models import model_from_json

#model_json = model.to_json()
#with open("nn_structure_and_weights/model.json", "w") as json_file:
#    json_file.write(model_json)

#model.save_weights("nn_structure_and_weights/weights.h5")

model.save('nn_structure_and_weights/model.h5', include_optimizer=False)

print("Model and weight saved to disk")

