import numpy as np
import pandas as pd
import random
import matplotlib.pyplot as plt
import matplotlib.tri as mtri

def add_gaussian_noise(signal):
    noise=np.random.normal(0,0.05,186)
    return (signal+noise)

ptbdb_abnormal = pd.read_csv("../../dataset/ptbdb_abnormal.csv", header = None)
ptbdb_normal = pd.read_csv("../../dataset/ptbdb_normal.csv", header = None)

data = []

data.extend(ptbdb_normal.iloc[:, :186].values)
data.extend(ptbdb_abnormal.iloc[:, :186].values)

labels = []
labels.extend(ptbdb_normal.iloc[:, 187].values)
labels.extend(ptbdb_abnormal.iloc[:, 187].values)

data = np.array(data)
labels = np.array(labels)

from keras.utils import to_categorical

one_hot_labels = np.zeros((len(labels), 2))
one_hot_labels[:] = to_categorical(labels[:], num_classes = 2)

from sklearn.model_selection import train_test_split

X_train, X_test, y_train, y_test = train_test_split(data, one_hot_labels, test_size=0.33, random_state=42)

from keras.models import Sequential
from keras.layers import Input, Dense, Activation, BatchNormalization, Dropout, Convolution1D, MaxPooling1D, SeparableConv1D, Flatten, MaxPool1D
from keras.optimizers import Adam
from keras.models import Model

########################
print('############################################\n')
print("Pocinje:\n")
print('############################################\n')

X_train[:] = add_gaussian_noise(X_train[:])#Small data augmentation
X_test[:] = add_gaussian_noise(X_test[:])#Small data augmentation

X_train = X_train.reshape(len(X_train), X_train.shape[1],1)
X_test = X_test.reshape(len(X_test), X_test.shape[1],1)

im_shape=(X_train.shape[1],1)
inputs_cnn=Input(shape=(im_shape), name='inputs_cnn')
conv1_1=Convolution1D(128, (6), activation='relu', input_shape=im_shape)(inputs_cnn)
conv1_1=BatchNormalization()(conv1_1)
pool1=MaxPool1D(pool_size=(3), strides=(2), padding="same")(conv1_1)
conv2_1=Convolution1D(64, (3), activation='relu', input_shape=im_shape)(pool1)
conv2_1=BatchNormalization()(conv2_1)
pool2=MaxPool1D(pool_size=(2), strides=(2), padding="same")(conv2_1)
conv3_1=Convolution1D(64, (3), activation='relu', input_shape=im_shape)(pool2)
conv3_1=BatchNormalization()(conv3_1)
pool3=MaxPool1D(pool_size=(2), strides=(2), padding="same")(conv3_1)
conv3_1=Convolution1D(32, (3), activation='relu', input_shape=im_shape)(pool2)
conv3_1=BatchNormalization()(conv3_1)
pool3=MaxPool1D(pool_size=(2), strides=(2), padding="same")(conv3_1)
flatten=Flatten()(pool3)
dense_end1 = Dense(64, activation='relu')(flatten)
dense_end2 = Dense(32, activation='relu')(dense_end1)
main_output = Dense(2, activation='softmax', name='main_output')(dense_end2)


model = Model(inputs= inputs_cnn, outputs=main_output)
########################

model.compile(optimizer = Adam(lr = 100e-5), loss = "categorical_crossentropy", metrics = ["accuracy"])

EPOCHS = 10
history = model.fit(X_train, y_train, validation_split = 0.2, epochs = EPOCHS, shuffle = True, class_weight = 'auto')

print(model.evaluate(X_test, y_test))

#N = np.arange(0, EPOCHS)
#title = "Training loss and Acuracy"

#plt.style.use("ggplot")
#plt.figure()
#plt.plot(N, history.history["loss"], label = "train_loss")
#plt.plot(N, history.history["val_loss"], label = "val_loss")
#plt.plot(N, history.history["accuracy"], label = "train_acc")
#plt.plot(N, history.history["val_accuracy"], label = "val_acc")
#plt.title(title)
#plt.xlabel("Epoch #")
#plt.ylabel("Loss/Accuracy")
#plt.legend()


from keras.models import model_from_json

#model_json = model.to_json()
#with open("nn_structure_and_weights/model.json", "w") as json_file:
#    json_file.write(model_json)

#model.save_weights("nn_structure_and_weights/weights.h5")

model.save('nn_structure_and_weights/model.h5', include_optimizer=False)

print("Model and weight saved to disk")

