import numpy as np
import pandas as pd
import random
import matplotlib.pyplot as plt
import matplotlib.tri as mtri

mitbih = pd.read_csv("../../dataset/mitbih_test.csv", header = None)
mitbih_train = pd.read_csv("../../dataset/mitbih_train.csv", header = None)

data = mitbih_train.iloc[:, :186]
labels = mitbih_train.iloc[:, 187]

from keras.utils import to_categorical

one_hot_labels = np.zeros((len(labels), 5))
one_hot_labels[:] = to_categorical(labels[:], num_classes = 5)

from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(data, one_hot_labels, test_size = 0.33, random_state = 42)

from keras.models import Sequential
from keras.layers import Dense, Activation, BatchNormalization, Dropout
from keras.optimizers import Adam

model = Sequential()

model.add(Dense(50, input_dim = 186, kernel_initializer = 'normal', activation = 'relu'))
model.add(Dropout(0.2))
model.add(Dense(30, kernel_initializer = 'normal', activation = 'relu'))
model.add(Dropout(0.2))
model.add(Dense(5,  kernel_initializer = 'normal', activation = 'softplus'))#softmax

model.compile(optimizer = Adam(lr = 100e-5), loss = "categorical_crossentropy", metrics = ["accuracy"])

EPOCH = 30
history = model.fit(X_train, y_train, validation_split = 0.2, epochs = EPOCH, shuffle = True, class_weight = 'auto')

print(model.evaluate(X_test, y_test))

N = np.arange(0, EPOCH)
title = "Training loss and Acuracy"

plt.style.use("ggplot")
plt.figure()
plt.plot(N, history.history["loss"], label = "train_loss")
plt.plot(N, history.history["val_loss"], label = "val_loss")
plt.plot(N, history.history["accuracy"], label = "train_acc")
plt.plot(N, history.history["val_accuracy"], label = "val_acc")
plt.title(title)
plt.xlabel("Epoch #")
plt.ylabel("Loss/Accuracy")
plt.legend()


#from keras.models import model_from_json

#model_json = model.to_json()
#with open("nn_structure_and_weights/model.json", "w") as json_file:
#    json_file.write(model_json)

#model.save_weights("nn_structure_and_weights/weights.h5")

model.save('nn_structure_and_weights/model.h5', include_optimizer=False)

print("Model and weight saved to disk")

