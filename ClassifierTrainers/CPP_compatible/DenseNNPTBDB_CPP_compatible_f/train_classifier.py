import numpy as np
import pandas as pd
import random
import matplotlib.pyplot as plt
import matplotlib.tri as mtri
import math

ptbdb_abnormal = pd.read_csv("../../dataset/ptbdb_abnormal.csv", header = None)
ptbdb_normal = pd.read_csv("../../dataset/ptbdb_normal.csv", header = None)

data = []

data.extend(ptbdb_normal.iloc[:, :186].values)
data.extend(ptbdb_abnormal.iloc[:, :186].values)

labels = []
labels.extend(ptbdb_normal.iloc[:, 187].values)
labels.extend(ptbdb_abnormal.iloc[:, 187].values)

data = np.array(data)
labels = np.array(labels)

from keras.utils import to_categorical

one_hot_labels = np.zeros((len(labels), 2))
one_hot_labels[:] = to_categorical(labels[:], num_classes = 2)

from sklearn.model_selection import train_test_split

X_train, X_test, y_train, y_test = train_test_split(data, one_hot_labels, test_size=0.33, random_state=42)

from keras.models import Sequential
from keras.layers import Dense, Activation,BatchNormalization,Dropout
from keras.optimizers import Adam

model = Sequential()

model.add(Dense(80, input_dim=186, init='normal', activation='relu'))
model.add(Dropout(0.2))
model.add(Dense(50, init='normal', activation='relu'))
model.add(Dropout(0.2))
model.add(Dense(30, init='normal', activation='relu'))
model.add(Dropout(0.2))
model.add(Dense(2, activation='softmax'))

model.compile(optimizer = Adam(lr = 100e-5), loss="categorical_crossentropy", metrics=["accuracy"])
#model.compile(optimizer='adam', loss='categorical_crossentropy', metrics=['acc',f1])

history = model.fit(X_train, y_train, validation_split = 0.2, epochs=30, shuffle=True, class_weight='auto')

print(model.evaluate(X_test, y_test))

#N = np.arange(0, 30)
#title = "Training Loss and Accuracy"
 
#plt.style.use("ggplot")
#plt.figure()
#plt.plot(N, history.history["loss"], label="train_loss")
#plt.plot(N, history.history["val_loss"], label="val_loss")
#plt.plot(N, history.history["accuracy"], label="train_acc")
#plt.plot(N, history.history["val_accuracy"], label="val_acc")
#plt.title(title)
#plt.xlabel("Epoch #")
#plt.ylabel("Loss/Accuracy")
#plt.legend()


#from keras.models import model_from_json

#model_json = model.to_json()
#with open("nn_structure_and_weights/model.json", "w") as json_file:
#    json_file.write(model_json)

#model.save_weights("nn_structure_and_weights/weights.h5")

model.save('nn_structure_and_weights/model.h5', include_optimizer=False)

print("Model and weight saved to disk")


