import numpy as np
import pandas as pd
import random
import matplotlib.pyplot as plt
import matplotlib.tri as mtri
import math

mitbih = pd.read_csv("../../dataset/mitbih_test.csv")
mitbih_train = pd.read_csv("../../dataset/mitbih_train.csv")

data = mitbih_train.iloc[:, :186].values
labels = mitbih_train.iloc[:, 187].values

from keras.utils import to_categorical

one_hot_labels = np.zeros((len(labels), 5))
one_hot_labels[:] = to_categorical(labels[:], num_classes = 5)

from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(data, one_hot_labels, test_size = 0.33, random_state = 42)

from keras.models import Sequential
from keras.layers import Input, Dense, Activation, BatchNormalization, Dropout, Convolution1D, MaxPooling1D, SeparableConv1D, Flatten, MaxPool1D
from keras.optimizers import Adam
from keras.models import Model

#X_train[:] = add_gaussian_noise(X_train[:])#Small data augmentation
#X_test[:] = add_gaussian_noise(X_test[:])#Small data augmentation

X_train = X_train.reshape(len(X_train), X_train.shape[1],1)
X_test = X_test.reshape(len(X_test), X_test.shape[1],1)

im_shape=(X_train.shape[1],1)
inputs_cnn=Input(shape=(im_shape), name='inputs_cnn')
conv1_1=Convolution1D(64, (6), activation='relu', input_shape=im_shape)(inputs_cnn)
conv1_1=BatchNormalization()(conv1_1)
pool1=MaxPool1D(pool_size=(3), strides=(2), padding="same")(conv1_1)
conv2_1=Convolution1D(64, (3), activation='relu', input_shape=im_shape)(pool1)
conv2_1=BatchNormalization()(conv2_1)
pool2=MaxPool1D(pool_size=(2), strides=(2), padding="same")(conv2_1)
conv3_1=Convolution1D(64, (3), activation='relu', input_shape=im_shape)(pool2)
conv3_1=BatchNormalization()(conv3_1)
pool3=MaxPool1D(pool_size=(2), strides=(2), padding="same")(conv3_1)
flatten=Flatten()(pool3)
dense_end1 = Dense(64, activation='relu')(flatten)
dense_end2 = Dense(32, activation='relu')(dense_end1)
main_output = Dense(5, activation='softmax', name='main_output')(dense_end2)


model = Model(inputs= inputs_cnn, outputs=main_output)

model.compile(optimizer = Adam(lr = 100e-5), loss = "categorical_crossentropy", metrics = ["accuracy"])

EPOCHS = 15
history = model.fit(X_train, y_train, validation_split = 0.2, epochs = EPOCHS, shuffle = True, class_weight = 'auto')

print(model.evaluate(X_test, y_test))

print("Treniranje neuralne mreze sa MIT_BIH datasetom je zavrseno")
# MITBIH treniranje zavrseno

ptbdb_abnormal = pd.read_csv("../../dataset/ptbdb_abnormal.csv", header = None)
ptbdb_normal = pd.read_csv("../../dataset/ptbdb_normal.csv", header = None)

data_ptb = []

data_ptb.extend(ptbdb_normal.iloc[:, :186].values)
data_ptb.extend(ptbdb_abnormal.iloc[:, :186].values)

labels_ptb = []
labels_ptb.extend(ptbdb_normal.iloc[:, 187].values)
labels_ptb.extend(ptbdb_abnormal.iloc[:, 187].values)

data_ptb = np.array(data_ptb)
labels_ptb = np.array(labels_ptb)

one_hot_labels_ptb = np.zeros((len(labels_ptb), 2))
one_hot_labels_ptb[:] = to_categorical(labels_ptb[:], num_classes = 2)

X_train_ptb, X_test_ptb, y_train_ptb, y_test_ptb = train_test_split(data_ptb, one_hot_labels_ptb, test_size=0.33, random_state=42)

X_train_ptb = X_train_ptb.reshape(len(X_train_ptb), X_train_ptb.shape[1],1)
X_test_ptb = X_test_ptb.reshape(len(X_test_ptb), X_test_ptb.shape[1],1)

dense_ptb1 = Dense(32, activation='relu')(model.layers[-3].output)
dense_ptb2 = Dense(32, activation='relu')(dense_ptb1)
dense_ptb3 = Dense(16, activation='relu')(dense_ptb2)
dense_ptb4 = Dense(8, activation='relu')(dense_ptb3)
dense_ptb5 = Dense(2, activation='softmax')(dense_ptb4)

model_final = Model(inputs=model.input, outputs=dense_ptb5)

for layer in model_final.layers[:-5]:
    layer.trainable = False

for layer in model_final.layers[-5:]:
    layer.trainable = True

model_final.compile(optimizer = Adam(lr = 100e-5), loss='categorical_crossentropy', metrics=['accuracy'])

EPOCHS_ptb = 50
history_ptb = model_final.fit(X_train_ptb, y_train_ptb, validation_split=0.15, epochs=EPOCHS_ptb, batch_size=128)

print(model_final.evaluate(X_test_ptb, y_test_ptb))

print("Treniranje neuralne mreze sa PTBDB datasetom je zavrseno")
# Treniranje neuralne mreze sa PTBDB datasetom je zavrseno

#from keras.models import model_from_json

#model_json = model_final.to_json()
#with open("nn_structure_and_weights/model.json", "w") as json_file:
#    json_file.write(model_json)

#model_final.save_weights("nn_structure_and_weights/weights.h5")

model_final.save('nn_structure_and_weights/model.h5', include_optimizer=False)

print("Model and weight saved to disk")


