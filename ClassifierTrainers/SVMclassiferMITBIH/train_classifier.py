import numpy as np
import pandas as pd
import random
import matplotlib.pyplot as plt
import matplotlib.tri as mtri
import math

mitbih = pd.read_csv("../dataset/mitbih_test.csv")
mitbih_train = pd.read_csv("../dataset/mitbih_train.csv")

data = mitbih_train.iloc[:, :186].values
labels = mitbih_train.iloc[:, 187].values

from keras.utils import to_categorical

one_hot_labels = np.zeros((len(labels), 5))
one_hot_labels[:] = to_categorical(labels[:], num_classes = 5)

from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(data, one_hot_labels, test_size = 0.33, random_state = 42)

from keras.models import Sequential
from keras.layers import Input, Dense, Activation, BatchNormalization, Dropout, Convolution1D, MaxPooling1D, SeparableConv1D, Flatten, MaxPool1D
from keras.optimizers import Adam
from keras.models import Model

#X_train[:] = add_gaussian_noise(X_train[:])#Small data augmentation
#X_test[:] = add_gaussian_noise(X_test[:])#Small data augmentation

X_train = X_train.reshape(len(X_train), X_train.shape[1],1)
X_test = X_test.reshape(len(X_test), X_test.shape[1],1)

im_shape=(X_train.shape[1],1)
inputs_cnn=Input(shape=(im_shape), name='inputs_cnn')
conv1_1=Convolution1D(64, (6), activation='relu', input_shape=im_shape)(inputs_cnn)
conv1_1=BatchNormalization()(conv1_1)
pool1=MaxPool1D(pool_size=(3), strides=(2), padding="same")(conv1_1)
conv2_1=Convolution1D(64, (3), activation='relu', input_shape=im_shape)(pool1)
conv2_1=BatchNormalization()(conv2_1)
pool2=MaxPool1D(pool_size=(2), strides=(2), padding="same")(conv2_1)
conv3_1=Convolution1D(64, (3), activation='relu', input_shape=im_shape)(pool2)
conv3_1=BatchNormalization()(conv3_1)
pool3=MaxPool1D(pool_size=(2), strides=(2), padding="same")(conv3_1)
flatten=Flatten()(pool3)
dense_end1 = Dense(64, activation='relu')(flatten)
dense_end2 = Dense(32, activation='relu')(dense_end1)
main_output = Dense(5, activation='softmax', name='main_output')(dense_end2)


model = Model(inputs= inputs_cnn, outputs=main_output)

model.compile(optimizer = Adam(lr = 100e-5),
              loss = "categorical_crossentropy",
              metrics = ['accuracy'])

EPOCHS = 5
history = model.fit(X_train, y_train, validation_split = 0.2, epochs = EPOCHS, shuffle = True, class_weight = 'auto')

print(model.evaluate(X_test, y_test))

#---------------------------------------

model_final = Model(inputs=model.input, outputs=model.layers[-2].output)
model_final.compile(optimizer = Adam(lr = 100e-5), loss='categorical_crossentropy', metrics=['accuracy'])

representations = []

dataSVM = np.array(data.copy())

dataSVM = dataSVM.reshape(len(dataSVM), dataSVM.shape[1],1)

for i in range(len(mitbih_train)):
    representations.append(model_final.predict(np.array([dataSVM[i]])))

representations = np.array(representations).squeeze()

X_train_SVM, X_test_SVM, y_train_SVM, y_test_SVM = train_test_split(representations, one_hot_labels, test_size = 0.33, random_state = 42)

from sklearn import svm
from sklearn.multiclass import OneVsRestClassifier
from sklearn.svm import LinearSVC

clf = OneVsRestClassifier(LinearSVC(dual=False))

clf.fit(X_train_SVM, y_train_SVM)

#y_pred = clf.decision_function(X_test_SVM)

#rectified_prediction = np.where(y_pred < 0.5, 0, 1)

#---------------------------------------

from keras.models import model_from_json

model_json = model_final.to_json()
with open("nn_structure_and_weights/model_representation.json", "w") as json_file:
    json_file.write(model_json)

model_final.save_weights("nn_structure_and_weights/weights_representation.h5")

import pickle
with open('nn_structure_and_weights/support_vectors.pkl', 'wb') as fid:
    pickle.dump(clf, fid)  

#with open('my_dumped_classifier.pkl', 'rb') as fid:
#    gnb_loaded = pickle.load(fid)

print("Model and weight saved to disk")


