
#include <alsa/asoundlib.h>
#include <math.h>

#define PI 3.1415

static char *device = "default";            /* playback device */
unsigned char beep[5000];

int err;
unsigned int i;
snd_pcm_t *handle;
snd_pcm_sframes_t frames;

void initBeep(void)
{
    for (i = 0; i < sizeof(beep); i++)
        beep[i] = (int)(sin(2*PI*500*i/48000) * 255.0);

    if ((err = snd_pcm_open(&handle, device, SND_PCM_STREAM_PLAYBACK, 0)) < 0)
    {
        printf("Playback open error: %s\n", snd_strerror(err));
        exit(EXIT_FAILURE);
    }

    if ((err = snd_pcm_set_params(handle,
                      SND_PCM_FORMAT_U8,
                      SND_PCM_ACCESS_RW_INTERLEAVED,
                      1,
                      48000,
                      1,
                      10000)) < 0)
    {   /* 0.1sec */
        printf("Playback open error: %s\n", snd_strerror(err));
        exit(EXIT_FAILURE);
    }
}

void closeBeep(void)
{
    snd_pcm_close(handle);
}

void playBeep(void)
{
    frames = snd_pcm_writei(handle, beep, sizeof(beep));
    if (frames < 0)
        frames = snd_pcm_recover(handle, frames, 0);
    if (frames < 0)
    {
        printf("snd_pcm_writei failed: %s\n", snd_strerror(frames));
        return;
    }
    if (frames > 0 && frames < (long)sizeof(beep))
    {
        printf("Short write (expected %li, wrote %li)\n", (long)sizeof(beep), frames);
    }
}