
#include "GL/freeglut.h"
#include "GL/glut.h"
#include "GL/gl.h"

#include <stdio.h>
#include <stdlib.h>

#include <pthread.h>

extern int argc;
extern char* argv[];

extern char SEGMENT_DATA;
extern int DATASET_LENGTH;

extern int SAMPLE_POINTS;
extern float data[];
extern double copy_of_hearthbeat[];

extern double BPM;
char BPMstring[20];
    


static char window_name[60] = "Vizuelizacija EKG signala\0";

/*
    Later add option to draw strings to screen
    then it will be possible to print BPM and other ECG charachteristics
*/

void drawLine(void)
{
    glClearColor(0.4, 0.4, 0.4, 0.4);
    glClear(GL_COLOR_BUFFER_BIT);

    glColor3f(1.0, 0.0, 0.0);
    glOrtho(-1.0, 1.0, -1.0, 1.0, -1.0, 1.0);

    for(int i = 1; i < SAMPLE_POINTS; i++)
    {
        glBegin(GL_LINES);
            glVertex2f(1.0 - 2 * float(i - 1)/SAMPLE_POINTS, 2 * data[i - 1] + 0.2);
            glVertex2f(1.0 - 2 * float(i)/SAMPLE_POINTS, 2 * data[i] + 0.2);
        glEnd();
    }
    
    if(SEGMENT_DATA)
    {
        glColor3f(1.0, 1.0, 1.0);
        for(int i = 1; i < DATASET_LENGTH; i++)
        {
            glBegin(GL_LINES);
                glVertex2f(0.5 - float(i - 1)/DATASET_LENGTH, copy_of_hearthbeat[DATASET_LENGTH - i] - 0.8);
                glVertex2f(0.5 - float(i)/DATASET_LENGTH, copy_of_hearthbeat[DATASET_LENGTH - i - 1] - 0.8);
            glEnd();
        }
    }

    glColor3f(1.0, 1.0, 1.0); 
    glRasterPos2f(-0.9, -0.9);

    sprintf(BPMstring, "BPM: %f", BPM);
    glutBitmapString(GLUT_BITMAP_9_BY_15, (const unsigned char*)BPMstring);

    glFlush();
}

void ECG_display(int* argc, char* argv[])
{
    glutInit(argc, argv);

    glutInitDisplayMode(GLUT_SINGLE);//GLUT_SINGLE
    glutInitWindowSize(500, 500);
    glutInitWindowPosition(100, 100);

    glutCreateWindow(window_name);

    glutDisplayFunc(drawLine);
    glutIdleFunc(drawLine);
    glutMainLoop();
}