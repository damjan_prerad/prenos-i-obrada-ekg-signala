Softwer se izvrsava na obradnoj jedinici.
ECG_acquisition je program pisan u C++ programskom jeziku namjenjen za prikupljanje podataka dobijenog sa nosivog e-Health senzora.
Podaci se salju preko Bluetooth-a koristenjem rfcomm protokola brzinom 115200 bita po sekundi.
Primljeni su odmjerci EKG signala rezolucije 12bita razvojeni znakom '\n'.
ECG_acquisition radi samo na Linux operativnom sistemu (provjereno samo na Ubuntu OS).
Prije pokretanja programa potrebno je povezati se sa uredjajem za prikupljanje podataka.

Za povezivanje je potrebno ispratiti sledecu proceduru:
- Uvjeriti se da je uredjaj upaljen
- Ukljuciti bluetooth na racunaru sa kojim se povezujete
- Otovoriti novi terminal (podrazumijeva se rad na nekoj od Linux distribucija)
- U terminalu, koristeci naredbu hcitool scan, se dobija MAC adresa bluetooth modula (koristeni modul je HC-06)
- Sledeca naredba ostvaruje povezivanje sa uredjajem rfcomm connect 0 <MAC_adresa_uredjaja>
- Ostaviti taj terminal prozor otvorenm i sada otvoriti novi  

Nakon izvrsenih koraka moze da se pokrene ECG_acq izvrsni fajl
- U novom terminal prozoru izvrsiti sudo ./ECG_acq

Sudo je potrebno jer ECG_receive otvara fajl u /dev direktorijumu za sta su potrebne admin dozvole.

Na uredjaju je moguce izabrati jednu od 4 frekvencije odmjeravanja, a to su 125Hz, 250Hz, 500Hz i 1000Hz.

Kompilacija ECG_receive.cpp fajla se postize izvrsavanjem naredbe:
g++ -o ECG_acq ECG_ui.cpp ECG_visual.cpp ECG_acquisition.cpp ECG_separation.cpp -lglut -lGL -lGLU -lpthread

Prilikom izvrsavnja programa ECG_acq pozeljno je iskoristiti pipe i prosljediti njegov izlaz na neki od klasifikatora (uz odgovarajuce parametre prosljedjene u komandnoj liniji).

ECG_SOURCE MAC address 00:00:12:09:36:43

sudo ./ECG_acq -P -N -V -F2
