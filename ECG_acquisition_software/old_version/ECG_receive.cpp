//compile with
//g++ test.cpp -lglut -lGL -lGLU -lpthread -o test


//execute with
//sudo ./cpp_backbone/ECG_receive <filename> | python train_classifier/real_time_classification.py
#include "GL/freeglut.h"
#include "GL/gl.h"

#include <math.h>

#include <stdio.h>
#include <stdlib.h>

#include <pthread.h>

#include <limits.h>

char *filename;

//char redraw_flag = 0;

#define SAMPLE_POINTS 186
float data[SAMPLE_POINTS] = {0.0};

//--------------------------------------
#define QRS_threshold 2800//Needs fine tuning
#define NO_RESET 15//Needs fine tuning
#define DO_RESET 15//Needs fine tuning
#define DATASET_LENGTH 186//744//374
#define BUFFER_SIZE 5

int heartbeat[DATASET_LENGTH];
double copy_of_hearthbeat[DATASET_LENGTH];


void decimateHearthbeat()
{
    //This is done in python
}

void outputHearthbeat(void)
{
    double max = 0;
    for(int i = 0; i < DATASET_LENGTH; i++)
    {
        if(copy_of_hearthbeat[i] > max) max = copy_of_hearthbeat[i];
    }
    for(int i = 0; i < DATASET_LENGTH; i++)
    {
        copy_of_hearthbeat[i] = copy_of_hearthbeat[i] / max;
    }

    for(int i = 0; i < DATASET_LENGTH -1; i++)
        printf("%f ", copy_of_hearthbeat[i]);// / 4096.0);//Division by 4096.0 : normalization like this is not good
    printf("%f\n", copy_of_hearthbeat[DATASET_LENGTH - 1]);// / 4096.0);//Division by 4096.0 : normalization like this is not good
}

void heartbeatCorrection(int min)
{
    for(int i = 0; i < DATASET_LENGTH; i++)
        heartbeat[i] -= min;
}

void copyHeartheat(void)
{
    for(int i = 0; i < DATASET_LENGTH; i++)
        copy_of_hearthbeat[i] = heartbeat[i];
}

void hearthbeatExtractionFSM(int sample)
{
    //Take every other sample... trying something
    static int even = 0;
    even ++;
    if(even % 2 == 0)
    {
        even = 0;
        return;
    }
    //

    static int state = 0;
    static int sampleCounter = 0;
    static int countDown = NO_RESET;
    static int countReset = DO_RESET;
    static int countBuffer = BUFFER_SIZE;
    
    static int min = INT_MAX;
    
    switch(state)
    {
        case 0:
            min = INT_MAX;
            if(sample < min) min = sample;
            sampleCounter = 0;
            countDown = NO_RESET;
            countReset = DO_RESET;
            countBuffer = BUFFER_SIZE;
            heartbeat[sampleCounter++] = sample;
            if(sample > QRS_threshold)
            {
                state = 1;
            }
        break;
        case 1:
            if(sample < min) min = sample;
            heartbeat[sampleCounter++] = sample;
            countDown --;
            if(countDown <= 0)
            {
                state = 2;
            }
        break;
        case 2:
            if(sample < min) min = sample;
            heartbeat[sampleCounter++] = sample;
            countReset--;
            if(sample > QRS_threshold)
            {
                state = 0;
            }
            if(countReset <= 0)
            {
                state = 3;
            }
        break;     
        case 3:
            heartbeat[sampleCounter++] = sample;
            if(sample < min) min = sample;
            if(sample >= QRS_threshold)
            {
                state = 4;
            }
            if(sampleCounter > DATASET_LENGTH)
            {
                heartbeatCorrection(min);
                copyHeartheat();
                outputHearthbeat();
                state = 0;
            }
        break;   
        case 4:
            heartbeat[sampleCounter++] = sample;
            if(sample < min) min = sample;
            countBuffer--;
            if(countBuffer <= 0)
            {
                heartbeatCorrection(min);
                for(int i = sampleCounter; i < DATASET_LENGTH; i++)
                {
                    heartbeat[i] = 0;
                }
                copyHeartheat();
                outputHearthbeat();
                state = 0;
            }
            if(sampleCounter > DATASET_LENGTH)
            {
                heartbeatCorrection(min);
                copyHeartheat();
                outputHearthbeat();
                state = 0;
            }            
        break;
    }
}
//--------------------------------------

void insertSample(float sample)
{
    for(int i = SAMPLE_POINTS - 1; i > 0; i--)
    {
        data[i] = data[i - 1];
    }
    data[0] = sample;
}

void drawLine()
{
    //if(redraw_flag == 1)
    //{
    //    redraw_flag = 0;
    //    glutPostRedisplay();
    //}
    glClearColor(0.4, 0.4, 0.4, 0.4);
    glClear(GL_COLOR_BUFFER_BIT);

    glColor3f(1.0, 1.0, 1.0);
    glOrtho(-1.0, 1.0, -1.0, 1.0, -1.0, 1.0);

    for(int i = 1; i < SAMPLE_POINTS; i++)
    {
        glBegin(GL_LINES);
            glVertex2f(1.0 - 2 * float(i - 1)/SAMPLE_POINTS, 2 * data[i - 1] + 0.2);//sin(2*3.1415*float(i - 1)/SAMPLE_POINTS));
            glVertex2f(1.0 - 2 * float(i)/SAMPLE_POINTS, 2 * data[i] + 0.2);//sin(2*3.1415*float(i)/SAMPLE_POINTS));
        glEnd();
    }

    for(int i = 1; i < DATASET_LENGTH; i++)
    {
        glBegin(GL_LINES);
            glVertex2f(0.5 - float(i - 1)/DATASET_LENGTH, copy_of_hearthbeat[DATASET_LENGTH - i] - 0.8);//sin(2*3.1415*float(i - 1)/SAMPLE_POINTS));
            glVertex2f(0.5 - float(i)/DATASET_LENGTH, copy_of_hearthbeat[DATASET_LENGTH - i - 1] - 0.8);//sin(2*3.1415*float(i)/SAMPLE_POINTS));
        glEnd();
    }

    glFlush();
}

void* updateFunc(void*)
{
    FILE *rfcomm_file;
    char str[60];

    rfcomm_file = fopen ("/dev/rfcomm0","r");
    if(rfcomm_file == NULL) {
      perror("Error opening rfcomm file");
      return NULL;
    }

    FILE *store_file;
    if(filename[0] != '0')
    {
        printf("%s", filename);
        store_file = fopen (filename,"w");
        if(rfcomm_file == NULL) {
        perror("Error opening store_file");
        return NULL;
        }
    }

    while(1){
        char c;
        int i = 0;
        while ((c = fgetc(rfcomm_file)) != '\n'){
            if(c == EOF) continue;
            str[i] = c;
            i++;
        }
        fgetc(rfcomm_file);//Wipe new line
        str[i] = '\0';
        int sample = atoi(str);
        
        insertSample(-0.5 + sample/4096.0);
        if(filename[0] != '0')
        {
            fprintf(store_file, "%s,", str);//Store data as .csv file
        }
        //FSM
        hearthbeatExtractionFSM(sample);
        //redraw_flag = 1;
        //maybe wait until a buffer is full and then print it
        //printf("%d\n",sample);
    }
    //Code never comes to this place... needs fixing!!!
    fclose(rfcomm_file);
    if(filename[0] != '0')
    {
        fclose(store_file);
    }
}

int main(int argc, char **argv)
{   
    if(argc < 2)
    {
        printf("Nedovoljno argumenata u komandnoj liniji\n");
        return 0;
    }
    if(argv[1][0] == 'h')
    {
        printf("Program je napravljen u svrhe predmeta strucna prksa\n");
        printf("na elektrotehnickom fakultetu u Banjojluci\n");
        printf("Kreator: Damjan Prerad\n");
        printf("2020. godine.\n");
        printf("Ako je prvi argument komandne linije:\n");
        printf("0 - ne upisuj u fajl\n");
        printf("h - prikazi ovaj tekst\n");
        printf("Bilo koji string koji ne pocnje sa h, EKG se upisuje u fajl pod tim nazivom.\n");
        return 0;
    }
    filename = argv[1];
    glutInit(&argc, argv);

    glutInitDisplayMode(GLUT_SINGLE);
    glutInitWindowSize(500, 500);
    glutInitWindowPosition(100, 100);
    glutCreateWindow("Vizuelizacija EKG signala");
    
    pthread_t t;
    pthread_create(&t,NULL,updateFunc,NULL);

    glutDisplayFunc(drawLine);
    glutIdleFunc(drawLine);
    glutMainLoop();

    return 0;
}