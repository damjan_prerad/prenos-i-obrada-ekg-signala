

#include <climits>

#include <float.h>

#include <stdlib.h>
#include <stdio.h>

extern int SAMPLE_RATE_DIVISOR;
extern char STANDARD_OUTPUT;
extern char NORMALIZATION;
extern char SAVE_SEGMENTS;

static unsigned int segment_counter = 0;

int QRS_threshold = 2800;//Needs fine tuning
int NO_RESET = 15;//Needs fine tuning
int DO_RESET = 15;//Needs fine tuning
int DATASET_LENGTH = 186;//744//374
int BUFFER_SIZE = 5;

static int state = 0;
static int sampleCounter = 0;
static int countDown = NO_RESET;
static int countReset = DO_RESET;
static int countBuffer = BUFFER_SIZE;

static double min = DBL_MAX;

double heartbeat[1000];
double copy_of_hearthbeat[1000];

#define BUFFER_SIZE 20
double BPM = 0;
double BPM_buffer[BUFFER_SIZE] = {0};
int bpm_count = 0;

static char segfile_open_error[] = "Greska pri cuvanju segmenta. Da li postoji folder 'segments'?\n";

#include "keras_model.h"
#include <iostream>
#include <vector>

extern KerasModel model;
int type;

void heartbeatCorrection(double min)
{
    if(NORMALIZATION)
    {
        //Highly inefficient function, needs fixing

        for(int i = 0; i < DATASET_LENGTH; i++)
            if(heartbeat[i] > 0)
                heartbeat[i] -= min;

        //max can be found in the FSM but then min will be different min = min / max (floating point divvision)
        double max = 0;
        for(int i = 0; i < DATASET_LENGTH; i++)
        {
            if(heartbeat[i] > max) max = heartbeat[i];
        }
        //----------------------------

        for(int i = 0; i < DATASET_LENGTH; i++)
            heartbeat[i] /= max;
    }
}

void copyHeartheat(void)
{
    //TODO: Add an option to store individual hearthbeats!!!
    for(int i = 0; i < DATASET_LENGTH; i++)
        copy_of_hearthbeat[i] = heartbeat[i];
}

/*Testing purposes*/
template <typename T, typename A>
int arg_max(std::vector<T, A> const& vec) {
  return static_cast<int>(std::distance(vec.begin(), max_element(vec.begin(), vec.end())));
}
/*Testing purposes*/

void outputHearthbeat(void)
{
    FILE *seg_file;
    if(SAVE_SEGMENTS)
    {
        char fn[30];
        sprintf(fn, "segments/%d", segment_counter);
        segment_counter ++;
        //printf("%s", filename);
        seg_file = fopen (fn,"w");
        if(seg_file == NULL) {
            perror(segfile_open_error);
            SAVE_SEGMENTS = 0;
        }
    }

    static Tensor in(186);
    std::vector<float> vec(copy_of_hearthbeat, copy_of_hearthbeat + 186);
    in.data_ = vec;

    // Run prediction.
    static Tensor out;
    model.Apply(&in, &out);
    out.Print();
    type = arg_max(out.data_);
    //printf("%f\n", out.data_[0]);

    if(STANDARD_OUTPUT)
    {


        for(int i = 0; i < DATASET_LENGTH -1; i++)
        {
            if(SAVE_SEGMENTS)
            {
                fprintf(seg_file, "%f,", copy_of_hearthbeat[i]);
            }
            printf("%f ", copy_of_hearthbeat[i]);
        }
        if(SAVE_SEGMENTS)
        {
            fprintf(seg_file, "%f,", copy_of_hearthbeat[DATASET_LENGTH - 1]);
        }
        printf("%f\n", copy_of_hearthbeat[DATASET_LENGTH - 1]);
    }

    if(SAVE_SEGMENTS)
    {
        fclose(seg_file);
    }
}

void hearthbeatExtractionFSM(int sample)
{
    //Time domain ECG hearthbeat separation algorithm

    //Take every other sample
    static int every_other = 0;
    every_other ++;
    if(every_other % SAMPLE_RATE_DIVISOR != 0)
    {
        return;
    }
    every_other = 0;
    //--------------------

    switch(state)
    {
        case 0:
            min = DBL_MAX;
            if(sample < min) min = sample;
            sampleCounter = 0;
            countDown = NO_RESET;
            countReset = DO_RESET;
            countBuffer = BUFFER_SIZE;
            heartbeat[sampleCounter++] = sample;
            if(sample > QRS_threshold)
            {
                state = 1;
            }
        break;
        case 1:
            if(sample < min) min = sample;
            heartbeat[sampleCounter++] = sample;
            countDown --;
            if(countDown <= 0)
            {
                state = 2;
            }
        break;
        case 2:
            if(sample < min) min = sample;
            heartbeat[sampleCounter++] = sample;
            countReset--;
            if(sample > QRS_threshold)
            {
                state = 0;
            }
            if(countReset <= 0)
            {
                state = 3;
            }
        break;     
        case 3:
            heartbeat[sampleCounter++] = sample;
            if(sample < min) min = sample;
            if(sample >= QRS_threshold)
            {
                state = 4;
            }
            if(sampleCounter > DATASET_LENGTH)
            {
                heartbeatCorrection(min);
                copyHeartheat();
                outputHearthbeat();
                state = 0;
            }
        break;   
        case 4:
            heartbeat[sampleCounter++] = sample;
            if(sample < min) min = sample;
            countBuffer--;
            if(countBuffer <= 0)
            {
                //BPM Calculation
                BPM_buffer[bpm_count] = sampleCounter / 125.0 * 60;//125Hz sampling rate, should be a variable
                bpm_count = (bpm_count + 1) % BUFFER_SIZE;
                
                BPM = 0.0;
                for(int i = 0; i < BUFFER_SIZE; i++)
                {
                   BPM += BPM_buffer[i];
                }
                BPM /= BUFFER_SIZE ;

                //---------------

                heartbeatCorrection(min);
                
                for(int i = sampleCounter; i < DATASET_LENGTH; i++)
                {
                    //Padd with zeros
                    heartbeat[i] = 0;
                }
                copyHeartheat();
                outputHearthbeat();
                state = 0;
            }
            if(sampleCounter > DATASET_LENGTH)
            {
                heartbeatCorrection(min);
                copyHeartheat();
                outputHearthbeat();
                state = 0;
            }            
        break;
    }
}