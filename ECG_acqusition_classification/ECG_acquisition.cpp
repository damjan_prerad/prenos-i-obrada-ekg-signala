


#include <pthread.h>

#include <stdio.h>
#include <stdlib.h>

extern char *filename;
extern char SEGMENT_DATA;
extern int DATASET_LENGTH;

extern void hearthbeatExtractionFSM(int);

//Needs to be put in a header file and be a DEFINE
int SAMPLE_POINTS = 1000;
float data[1000] = {0.0};

static char rfcomm_file[] = "/dev/rfcomm0";
static char rfcomm_open_error[] = "Greska pri otvaranju rfcomm fajla\n";

static char filename_open_error[] = "Greska pri otvaranju fajla za cuvanje podataka";

void insertSample(float sample)
{
    //This is inefficient, circular buffer is better suited for this
    for(int i = SAMPLE_POINTS - 1; i > 0; i--)
    {
        data[i] = data[i - 1];
    }
    data[0] = sample;
}

void* updateFunc(void*)
{
    FILE *rfcomm_file;
    char str[60];

    rfcomm_file = fopen ("/dev/rfcomm0","r");//rfcomm_file
    if(rfcomm_file == NULL) {
      perror(rfcomm_open_error);
      return NULL;
    }

    FILE *store_file;
    if(filename != NULL)
    {
        //printf("%s", filename);
        store_file = fopen (filename,"w");
        if(rfcomm_file == NULL) {
            perror(filename_open_error);
            return NULL;
        }
    }

    while(1){
        char c;
        int i = 0;
        while ((c = fgetc(rfcomm_file)) != '\n'){
            if(c == EOF) continue;
            str[i] = c;
            i++;
        }
        fgetc(rfcomm_file);//Wipe new line
        str[i] = '\0';
        int sample = atoi(str);
        
        insertSample(-0.5 + sample/4096.0);
        if(filename != NULL)
        {
            fprintf(store_file, "%s,", str);//Store data as .csv file
        }
        //FSM
        if(SEGMENT_DATA)
        {
            hearthbeatExtractionFSM(sample);
        }
        //maybe wait until a buffer is full and then print it
        //printf("%d\n",sample);
    }
    //Code never comes to this place... needs fixing!!!
    fclose(rfcomm_file);
    if(filename != NULL)
    {
        fclose(store_file);
    }
}

void ECG_acquisition(void)
{
    pthread_t t;
    pthread_create(&t,NULL,updateFunc,NULL);
}