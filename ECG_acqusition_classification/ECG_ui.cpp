
/*
Autor: Damjan Prerad
Godina: 2020.
*/


/*
Fajl ECG_ui.cpp je zaduzen za interfejs sa korisnikom

Ovdje se interpretiraju parametri prosledjeni sa komandne linije
Opcije su sledeci:

-O<naziv_fajla> -> procitani EKG signal ce da se sacuva u csv fajl sa nazivom koji se nalazi odmah iz znaka O (O kao Ograda)
                   primjetiti da naziv nije odvojen nikakvim seperatorom nego odmah slijedi iz znaka O
                   Ukoliko argument O nije naveden procitani EKG signal se nece cuvati

-S              -> segemntisani EKG odmjerci se cuvaju u folderu pod nazivom segments
                   potrebno ih je premjestiti u drugi folder prije novog pokretanja jer ce sacuvani fajlovi biti prepisani
                   Ova opcija ne moze da se pozove bez opcije -M

-P              -> omogucuje vizuelni prikaz primljenog singala

-F              -> uzimanje svakog n-tog odmjerka (podrazumijevano je svaki odmjerak).
                   Ova opcija se koristi ako je potrebno smanjiti frekvenciju odmjeravanja EKG signala
                   NAPOMENA!!! Ovo nije zamjena za ispravan algoritam decimacije

-V              -> primljeni signal se razdvaja na pojedinacne otkucaje srca

-I              -> razdvojeni otkucaji srca se prosljeduju na standardni izlaz

-N              -> normalizacija signala ukljucena (signal se skalira na opseg od 0 do 1)

-W              -> Vrijednost parametra QRS_threshold, podrazumijevano je 2800

-E              -> Vrijednost parametra NO_RESET, podrazumijevano je 15

-R              -> Vrijednost parametra DO_RESET, podrazumijevano je 15

-T              -> Vrijednost parametra DATASET_LENGTH, podrazumijevano je 186, ovo je maksimalna duzina jednog otkucaja srca

-Y              -> Vrijednost parametra BUFFER_SIZE, podrazumijevano je 5

-H              -> Pomoc (Help)

*/


#include <cstdio>
#include <cstdlib>
#include <iostream>

#include "keras_model.h"

KerasModel model;

using namespace std;

static char arguments_error_SV[150] = "Greska, ne moze opcija S biti aktivna ako opcija V nije aktivna.\n";

static char help_string[20000] = {
        "Autor: Damjan Prerad\n"
        "Godina 2020.\n"
        "Ovaj program je napisan kao dio sistema za prikupljanje i klasifikaciju EKG signala sa nosivog senzora\n"
        "Sistem je razvijan na predmetu Strucna praksa, projekat iz multimedijalnih signala i sistema i diplomski rad\n"
        "na elektrotehnickom fakultetu u Banjoj Luci\n"
        "\n"
        "-O<naziv_fajla> -> procitani EKG signal ce da se sacuva u csv fajl sa nazivom koji se nalazi odmah iza znaka O (O kao Ograda)\n"
        "                   primjetiti da naziv nije odvojen nikakvim seperatorom nego odmah slijedi iz znaka O\n"
        "                   Ukoliko argument O nije naveden procitani EKG signal se nece cuvati\n"

        "-S              -> segemntisani EKG odmjerci se cuvaju u folderu pod nazivom segments\n"
        "                   potrebno ih je premjestiti u drugi folder prije novog pokretanja jer ce sacuvani fajlovi biti prepisani\n"
        "                   Ova opcija ne moze da se pozove bez opcije -M\n"

        "-P              -> omogucuje vizuelni prikaz primljenog singala\n"

        "-F              -> uzimanje svakog n-tog odmjerka (podrazumijevano je svaki odmjerak).\n"
        "                   Ova opcija se koristi ako je potrebno smanjiti frekvenciju odmjeravanja EKG signala\n"
        "                   NAPOMENA!!! Ovo nije zamjena za ispravan algoritam decimacije\n"

        "-V              -> primljeni signal se razdvaja na pojedinacne otkucaje srca\n"

        "-I              -> razdvojeni otkucaji srca se prosljeduju na standardni izlaz\n"

        "-N              -> normalizacija signala ukljucena (signal se skalira na opseg od 0 do 1)\n"

        "-W<vrijednost>  -> Vrijednost parametra QRS_threshold, podrazumijevano je 2800\n"

        "-E<vrijednost>  -> Vrijednost parametra NO_RESET, podrazumijevano je 15\n"

        "-R<vrijednost>  -> Vrijednost parametra DO_RESET, podrazumijevano je 15\n"

        "-T<vrijednost>  -> Vrijednost parametra DATASET_LENGTH, podrazumijevano je 186, ovo je maksimalna duzina jednog otkucaja srca\n"
        "                   (ogranicenje je 1000)\n"

        "-Y<vrijednost>  -> Vrijednost parametra BUFFER_SIZE, podrazumijevano je 5\n"

        "-H              -> Pomoc (Help)\n"
};

static char flags[256] = { 0 }; // Command line flags

char* filename = NULL;

extern int QRS_threshold;
extern int NO_RESET;
extern int DO_RESET;
extern int DATASET_LENGTH;
extern int BUFFER_SIZE;

char NORMALIZATION = 0;
int SAMPLE_RATE_DIVISOR = 1;

char SHOW_DATA = 0;
char SAVE_SEGMENTS = 0;
char SEGMENT_DATA = 0;
char STANDARD_OUTPUT = 0;

extern void ECG_display(int* argc, char* argv[]);
extern void ECG_acquisition(void);

int main(int argc, char* argv[])
{
    for (int i = 1; i < argc; i++)
    {
        flags[argv[i][1]] = i;
    }

    if(flags['H'])
    {
        cout << help_string;
        return 1;
    }

    if(flags['O'])
    {
        filename = &(argv[flags['O']][2]);
    }

    if(flags['S'] > 0 && flags['V'] == 0)
    {
        cout << arguments_error_SV;

        return 0;
    }

    if(flags['W'])
    {
        QRS_threshold = atoi(&(argv[flags['W']][2]));
    }

    if(flags['E'])
    {
        NO_RESET = atoi(&(argv[flags['E']][2]));
    }

    if(flags['R'])
    {
        DO_RESET = atoi(&(argv[flags['R']][2]));
    }

    if(flags['T'])
    {
        DATASET_LENGTH = atoi(&(argv[flags['T']][2]));
    }

    if(flags['Y'])
    {
        DATASET_LENGTH = atoi(&(argv[flags['Y']][2]));
    }

    if(flags['N'])
    {
        NORMALIZATION = 1;
    }

    if(flags['F'])
    {
        SAMPLE_RATE_DIVISOR = atoi(&(argv[flags['F']][2]));
    }

    if(flags['P'])
    {
        SHOW_DATA = 1;
    }

    if(flags['S'])
    {
        SAVE_SEGMENTS = 1;
    }
        
    if(flags['V'])
    {
        SEGMENT_DATA = 1;
    }

    if(flags['I'])
    {
        STANDARD_OUTPUT = 1;
    }
    //-------------------------------------------------------------------

    model.LoadModel("ClassifierTrainers/DenseNNMITBIH_CPP_compatible/nn_structure_and_weights/experimental.model");

    ECG_acquisition();
    if(SHOW_DATA)
    {
        ECG_display(&argc, argv);
    }
    //Below this, if show data is true code will not be executed... ECG_display initiates while(1) loop


    //Quick fix. Now you can only exit your program with ctrl + c
    //It is necessary to join threads insetead of while
    while(1){;}

    return 1;
}