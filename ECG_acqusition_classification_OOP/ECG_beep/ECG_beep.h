

#ifndef ECG_BEEP_H
#define ECG_BEEP_H

#include <SFML/Audio.hpp>
#include <math.h>
#include <pthread.h>

#include <stdint.h>

#define PI 3.141516

class ECG_beep
{

    private:
    sf::SoundBuffer buffer;
    sf::Sound sound;
    std::vector<sf::Int16> samples;

    public:
    void initECG_beep()
    {
        int16_t beep_sound[14700];
        for(int i = 0; i < 14700; i++)
        {
            beep_sound[i] = 32768 * sin(2 * PI * 300 * i/44100);
        }
        samples = new std::vector<sf::Int16>(beep_sound, beep_sound + 14700);
        buffer.loadFromSamples(&samples[0], samples.size(), 2, 44100);
        sound.setBuffer(buffer);
    }
    void ECG_beepOnce()
    {
        sound.play();
    }
}

#endif //ECG_BEEP_H