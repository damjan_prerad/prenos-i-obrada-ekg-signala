
#include <SFML/Audio.hpp>
#include <math.h>
#include <pthread.h>

#include <stdint.h>

int beep_signal = 0;

sf::SoundBuffer buffer;
sf::Sound sound;

#define PI 3.141516

void* beep_loop(void*)
{
    int16_t beep_sound[14700];
    for(int i = 0; i < 14700; i++)
    {
        beep_sound[i] = 32768 * sin(2 * PI * 300 * i/44100);
    }
    std::vector<sf::Int16> samples(beep_sound, beep_sound + 14700);
    buffer.loadFromSamples(&samples[0], samples.size(), 2, 44100);
    sound.setBuffer(buffer);

    while(1)
    {
        while(beep_signal == 0);
        sound.play();
        beep_signal = 0;
    }


    return 0;
}

void ECG_beep(void)
{
    pthread_t t;
    pthread_create(&t,NULL,beep_loop,NULL);
}