

#ifndef ECG_PREDICTION_H
#define ECG_PREDICTION_H

#include <climits>

#include <float.h>
#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>

#include "include/fdeep/fdeep.hpp"

class ECG_prediction
{
    private:
    const char *ecg_class_name_mitbih[5] = {
        "Normalan",
        "Supraventricularan ectopicni",
        "Ventrikularan ektopicni",
        "Spojen",
        "Nepoznat"
    };
    const char *ecg_class_name_ptbdb[5] = {
        "Normalan",
        "Abnormalan"
    };

    const char *ecg_class_n;

    const auto modelA = fdeep::load_model("ClassifierTrainers/CPP_compatible/DenseNNMITBIH_CPP_compatible_f/nn_structure_and_weights/model.json");
    const auto modelB = fdeep::load_model("ClassifierTrainers/CPP_compatible/ConvolutionalNNMITBIH_CPP_compatible_f/nn_structure_and_weights/model.json");
    const auto modelC = fdeep::load_model("ClassifierTrainers/CPP_compatible/DenseNNPTBDB_CPP_compatible_f/nn_structure_and_weights/model.json");
    const auto modelD = fdeep::load_model("ClassifierTrainers/CPP_compatible/ConvolutionalNNPTBDB_CPP_compatible_f/nn_structure_and_weights/model.json");
    const auto modelE = fdeep::load_model("ClassifierTrainers/CPP_compatible/TransferLearningMITBIHPTBDB_CPP_compatible_f/nn_structure_and_weights/model.json");

    fdeep::float_vec one_result = {};

    int (*prediction_ptr)();

    template <typename T, typename A>
    int arg_max(std::vector<T, A> const& vec) {
        return static_cast<int>(std::distance(vec.begin(), max_element(vec.begin(), vec.end())));
    }

    public:
    ECG_prediction(uit8_t modelSelector)
    {
        switch(modelSelector)
        {
            case 1:
                prediction_ptr = modelA();
            break;
            case 2:
                prediction_ptr = modelB();
            break;
            case 3:
                prediction_ptr = modelC();
            break;
            case 4:
                prediction_ptr = modelD();
            break;
            case 5:
                prediction_ptr = modelE();
            break;
            case 6:
                prediction_ptr = predictVoting();
            break;
            
        }
    }
    const char* getClass()
    {
        return ecg_class_n;
    }
    int predictModelA()
    {
        const auto result = modelA.predict(
        {fdeep::tensor(fdeep::tensor_shape(186), vec)});//Dense
        //{fdeep::tensor(fdeep::tensor_shape(186, 1), vec)});//CNN
        std::string s = fdeep::show_tensors(result);
        one_result =  *result[0].as_vector();
        double results_v[5] = {};
        results_v[0] = one_result[0];
        results_v[1] = one_result[1];
        results_v[2] = one_result[2];
        results_v[3] = one_result[3];
        results_v[4] = one_result[4];

        std::vector<float> vec_v(results_v, results_v + 5);
        
        ecg_class_n = ecg_class_name_mitbih[type1];

        std::cout << s << std::endl;
        return arg_max(vec_v);
    }

    int predictModelB()
    {
        const auto result = modelB.predict(
        //{fdeep::tensor(fdeep::tensor_shape(186), vec)});//Dense
        {fdeep::tensor(fdeep::tensor_shape(186, 1), vec)});//CNN
        std::string s = fdeep::show_tensors(result);
        one_result =  *result[0].as_vector();
        double results_v[5] = {};
        results_v[0] = one_result[0];
        results_v[1] = one_result[1];
        results_v[2] = one_result[2];
        results_v[3] = one_result[3];
        results_v[4] = one_result[4];

        std::vector<float> vec_v(results_v, results_v + 5);
        
        ecg_class_n = ecg_class_name_mitbih[type2];

        std::cout << s << std::endl;
        return arg_max(vec_v);
    }

    int predictModelC()
    {
        const auto result = modelC.predict(
        {fdeep::tensor(fdeep::tensor_shape(186), vec)});//Dense
        //{fdeep::tensor(fdeep::tensor_shape(186, 1), vec)});//CNN
        std::string s = fdeep::show_tensors(result);
        one_result =  *result[0].as_vector();
        double results_v[2] = {};
        results_v[0] = one_result[0];
        results_v[1] = one_result[1];

        std::vector<float> vec_v(results_v, results_v + 2);

        std::cout << s << std::endl;
        //ecg_class_n = ecg_class_name_ptbdb[type3];
        return arg_max(vec_v);
    }

    int predictModelD()
    {
        const auto result = modelD.predict(
        //{fdeep::tensor(fdeep::tensor_shape(186), vec)});//Dense
        {fdeep::tensor(fdeep::tensor_shape(186, 1), vec)});//CNN
        std::string s = fdeep::show_tensors(result);
        one_result =  *result[0].as_vector();
        double results_v[2] = {};
        results_v[0] = one_result[0];
        results_v[1] = one_result[1];

        std::vector<float> vec_v(results_v, results_v + 2);
        
        if(allTogether_flag == 0)
        {
            std::cout << s << std::endl;
        }
        //ecg_class_n = ecg_class_name_ptbdb[type4];
        return arg_max(vec_v);
    }

    int predictModelE()
    {
        const auto result = modelE.predict(
        //{fdeep::tensor(fdeep::tensor_shape(186), vec)});//Dense
        {fdeep::tensor(fdeep::tensor_shape(186, 1), vec)});//CNN
        std::string s = fdeep::show_tensors(result);
        one_result =  *result[0].as_vector();
        double results_v[2] = {};
        results_v[0] = one_result[0];
        results_v[1] = one_result[1];

        std::vector<float> vec_v(results_v, results_v + 2);
        
        if(allTogether_flag == 0)
        {
            //printf("%d\n", type5);
            std::cout << s << std::endl;
        }
        //ecg_class_n = ecg_class_name_ptbdb[type5];
        return arg_max(vec_v);
    }

    int predictVoting()
    {   
        int c = predictModelC();
        int d = predictModelD();
        int e = predictModelE();

        if(c == d)
        {
            ecg_class_n = ecg_class_name_ptbdb[c];
            return c;
        }
        if(c == e)
        {
            ecg_class_n = ecg_class_name_ptbdb[c];
            return c;
        }
        if(d == e)
        {
            ecg_class_n = ecg_class_name_ptbdb[d];
            return d;
        }
    }
    
    int predict()
    {
        return prediction_ptr();
    }
}

#endif //ECG_PREDICTION_H