#include <climits>

#include <float.h>
#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>

#include "include/fdeep/fdeep.hpp"

extern std::vector<float> vec;

int modelA_flag = 0;//MIT-BIH DNN
int modelB_flag = 0;//MIT-BIH CNN
int modelC_flag = 0;//PTB-DB DNN
int modelD_flag = 0;//PTB-DB CNN
int modelE_flag = 0;//PTB-DB TransferLearning
int allTogether_flag = 0;//modelC, modelD, modelE


int type1;
int type2;
int type3;
int type4;
int type5;

const char *ecg_class_name_mitbih[5] = {
    "Normalan",
    "Supraventricularan ectopicni",
    "Ventrikularan ektopicni",
    "Spojen",
    "Nepoznat"
};

const char *ecg_class_name_ptbdb[5] = {
    "Normalan",
    "Abnormalan"
};

const char *ecg_class_n;

const auto modelA = fdeep::load_model("ClassifierTrainers/CPP_compatible/DenseNNMITBIH_CPP_compatible_f/nn_structure_and_weights/model.json");
const auto modelB = fdeep::load_model("ClassifierTrainers/CPP_compatible/ConvolutionalNNMITBIH_CPP_compatible_f/nn_structure_and_weights/model.json");
const auto modelC = fdeep::load_model("ClassifierTrainers/CPP_compatible/DenseNNPTBDB_CPP_compatible_f/nn_structure_and_weights/model.json");
const auto modelD = fdeep::load_model("ClassifierTrainers/CPP_compatible/ConvolutionalNNPTBDB_CPP_compatible_f/nn_structure_and_weights/model.json");
const auto modelE = fdeep::load_model("ClassifierTrainers/CPP_compatible/TransferLearningMITBIHPTBDB_CPP_compatible_f/nn_structure_and_weights/model.json");

int signal = 0;

fdeep::float_vec one_result = {};
//double results_v[5] = {};

template <typename T, typename A>
int arg_max(std::vector<T, A> const& vec) {
  return static_cast<int>(std::distance(vec.begin(), max_element(vec.begin(), vec.end())));
}

void* predict(void*)
{
    while(1)//Propper exit mechanism must be implemented
    {
        while(signal == 0);

        type1 = type2 = type3 = type4 = type5 = -1;

        if(modelA_flag == 1)
        {
            const auto result = modelA.predict(
            {fdeep::tensor(fdeep::tensor_shape(186), vec)});//Dense
            //{fdeep::tensor(fdeep::tensor_shape(186, 1), vec)});//CNN
            std::string s = fdeep::show_tensors(result);
            one_result =  *result[0].as_vector();
            double results_v[5] = {};
            results_v[0] = one_result[0];
            results_v[1] = one_result[1];
            results_v[2] = one_result[2];
            results_v[3] = one_result[3];
            results_v[4] = one_result[4];

            std::vector<float> vec_v(results_v, results_v + 5);
            type1 = arg_max(vec_v);
            ecg_class_n = ecg_class_name_mitbih[type1];

            std::cout << s << std::endl;
        }

        if(modelB_flag == 1)
        {
            const auto result = modelB.predict(
            //{fdeep::tensor(fdeep::tensor_shape(186), vec)});//Dense
            {fdeep::tensor(fdeep::tensor_shape(186, 1), vec)});//CNN
            std::string s = fdeep::show_tensors(result);
            one_result =  *result[0].as_vector();
            double results_v[5] = {};
            results_v[0] = one_result[0];
            results_v[1] = one_result[1];
            results_v[2] = one_result[2];
            results_v[3] = one_result[3];
            results_v[4] = one_result[4];

            std::vector<float> vec_v(results_v, results_v + 5);
            type2 = arg_max(vec_v);
            ecg_class_n = ecg_class_name_mitbih[type2];

            std::cout << s << std::endl;
        }

        if(modelC_flag == 1 || allTogether_flag == 1)
        {
            const auto result = modelC.predict(
            {fdeep::tensor(fdeep::tensor_shape(186), vec)});//Dense
            //{fdeep::tensor(fdeep::tensor_shape(186, 1), vec)});//CNN
            std::string s = fdeep::show_tensors(result);
            one_result =  *result[0].as_vector();
            double results_v[2] = {};
            results_v[0] = one_result[0];
            results_v[1] = one_result[1];

            std::vector<float> vec_v(results_v, results_v + 2);
            type3 = arg_max(vec_v);

            if(allTogether_flag == 0)
            {
                std::cout << s << std::endl;
            }
            //ecg_class_n = ecg_class_name_ptbdb[type3];
        }

        if(modelD_flag == 1 || allTogether_flag == 1)
        {
            const auto result = modelD.predict(
            //{fdeep::tensor(fdeep::tensor_shape(186), vec)});//Dense
            {fdeep::tensor(fdeep::tensor_shape(186, 1), vec)});//CNN
            std::string s = fdeep::show_tensors(result);
            one_result =  *result[0].as_vector();
            double results_v[2] = {};
            results_v[0] = one_result[0];
            results_v[1] = one_result[1];

            std::vector<float> vec_v(results_v, results_v + 2);
            type4 = arg_max(vec_v);

            if(allTogether_flag == 0)
            {
                std::cout << s << std::endl;
            }
            //ecg_class_n = ecg_class_name_ptbdb[type4];
        }

        if(modelE_flag == 1 || allTogether_flag == 1)
        {
            const auto result = modelE.predict(
            //{fdeep::tensor(fdeep::tensor_shape(186), vec)});//Dense
            {fdeep::tensor(fdeep::tensor_shape(186, 1), vec)});//CNN
            std::string s = fdeep::show_tensors(result);
            one_result =  *result[0].as_vector();
            double results_v[2] = {};
            results_v[0] = one_result[0];
            results_v[1] = one_result[1];

            std::vector<float> vec_v(results_v, results_v + 2);
            type5 = arg_max(vec_v);

            if(allTogether_flag == 0)
            {
                //printf("%d\n", type5);
                std::cout << s << std::endl;
            }
            //ecg_class_n = ecg_class_name_ptbdb[type5];
        }

        if(allTogether_flag == 1)
        {
            if(type3 == type4)
            {
                ecg_class_n = ecg_class_name_ptbdb[type3];
            }
            if(type3 == type5)
            {
                ecg_class_n = ecg_class_name_ptbdb[type3];
            }
            if(type4 == type5)
            {
                ecg_class_n = ecg_class_name_ptbdb[type4];
            }
        }
        else
        {
            if(modelC_flag == 1)
            {
                ecg_class_n = ecg_class_name_ptbdb[type3];
            }
            if(modelD_flag == 1)
            {
                ecg_class_n = ecg_class_name_ptbdb[type4];
            }
            if(modelE_flag == 1)
            {
                ecg_class_n = ecg_class_name_ptbdb[type5];
            }
        }


        /*
        //in python data = data.reshape(1, 186, 1)
        const auto result = model.predict(
            {fdeep::tensor(fdeep::tensor_shape(186), vec)});
            //{fdeep::tensor(fdeep::tensor_shape(186, 1), vec)});
        std::string s = fdeep::show_tensors(result);
        one_result =  *result[0].as_vector();

        results_v[0] = one_result[0];
        results_v[1] = one_result[1];
        results_v[2] = one_result[2];
        results_v[3] = one_result[3];
        results_v[4] = one_result[4];

        std::vector<float> vec_v(results_v, results_v + 5);
        type = arg_max(vec_v);
        ecg_class_n = ecg_class_name[type];

        std::cout << s << std::endl;
        */
        signal = 0;
    }
}

void ECG_prediction(void)
{
    pthread_t t;
    pthread_create(&t,NULL,predict,NULL);
}