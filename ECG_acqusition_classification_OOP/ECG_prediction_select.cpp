#include <climits>

#include <float.h>
#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>

#include "include/fdeep/fdeep.hpp"

extern std::vector<float> vec;

int type;

const char *ecg_class_name[5] = {
    "Normalan",
    "Supraventricularan ectopicni",
    "Ventrikularan ektopicni",
    "Spojen",
    "Nepoznat"
};

const char *ecg_class_n;

const auto model = fdeep::load_model("ClassifierTrainers/CPP_compatible/ConvolutionalNNMITBIH_CPP_compatible_f/nn_structure_and_weights/model.json");
//const auto model = fdeep::load_model("ClassifierTrainers/CPP_compatible/DenseNNMITBIH_CPP_compatible_f/nn_structure_and_weights/model.json");


int signal = 0;

fdeep::float_vec one_result = {};
double results_v[5] = {};

template <typename T, typename A>
int arg_max(std::vector<T, A> const& vec) {
  return static_cast<int>(std::distance(vec.begin(), max_element(vec.begin(), vec.end())));
}

void* predict(void*)
{
    while(1)//Propper exit mechanism must be implemented
    {
        while(signal == 0);

        //in python data = data.reshape(1, 186, 1)
        const auto result = model.predict(
            {fdeep::tensor(fdeep::tensor_shape(186, 1), vec)});
        std::string s = fdeep::show_tensors(result);
        one_result =  *result[0].as_vector();

        results_v[0] = one_result[0];
        results_v[1] = one_result[1];
        results_v[2] = one_result[2];
        results_v[3] = one_result[3];
        results_v[4] = one_result[4];

        std::vector<float> vec_v(results_v, results_v + 5);
        type = arg_max(vec_v);
        ecg_class_n = ecg_class_name[type];

        std::cout << s << std::endl;

        signal = 0;
    }
}

void ECG_prediction(void)
{
    pthread_t t;
    pthread_create(&t,NULL,predict,NULL);
}