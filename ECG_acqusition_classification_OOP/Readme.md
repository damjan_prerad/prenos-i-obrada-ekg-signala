Objektno Orijentisani Pristup

Ovdje je spojen dio za akviziciju signala i njegovu klasifikaciju
Keras model treniran u pythonu se ucitava u C++ programskom jeziku i radi se predikcija na EKG zapisu.

ECG_SOURCE MAC address 00:00:12:09:36:43

Kerasify: g++ -o ECG_acq ECG_ui.cpp ECG_visual.cpp ECG_acquisition.cpp ECG_separation.cpp keras_model.cc -lglut -lGL -lGLU -lpthread

Frugally-deep: g++ -o ECG_acq ECG_ui.cpp ECG_visual.cpp ECG_acquisition.cpp ECG_separation.cpp ECG_prediction.cpp ECG_beep.cpp -lglut -lGL -lGLU -lpthread -lasound -Iinclude -lsfml-audio

Dodati -O3 radio brzeg izvrsavanja

sudo ./ECG_acqusition_classification_v2/ECG_acq -P -N -V -F2 -B -6
