


#include <pthread.h>

#include <stdio.h>
#include <stdlib.h>

extern char *filename;
extern char SEGMENT_DATA;
extern int DATASET_LENGTH;

extern void hearthbeatExtractionFSM(int);

//Needs to be put in a header file and be a DEFINE
int SAMPLE_POINTS = 1000;
float data[1000] = {0.0};

int notch;

static char rfcomm_file[] = "/dev/rfcomm0";
static char rfcomm_open_error[] = "Greska pri otvaranju rfcomm fajla\n";

static char filename_open_error[] = "Greska pri otvaranju fajla za cuvanje podataka";

void insertSample(float sample)
{
    //This is inefficient, circular buffer is better suited for this
    for(int i = SAMPLE_POINTS - 1; i > 0; i--)
    {
        data[i] = data[i - 1];
    }
    data[0] = sample;
}

double FIRnotchFilter(double sample)
{
    static double b0 = 2.6182;
    static double b1 = -4.2363;
    static double b2 = 2.6182;

    static double x1 = 0;
    static double x2 = 0;

    double y = b0 * sample + b1 * x1 + b2 * x2;
    
    x2 = x1;
    x1 = sample;
    return y;
}

double IIRnotchFilter(double sample)
{
    static double a0 = 1;
    static double a1 = -1.56873452;
    static double a2 = 0.93906251;

    static double b0 = 0.96953125;
    static double b1 = -1.56873452;
    static double b2 = 0.96953125;

    static double x1 = 0;
    static double x2 = 0;
    
    static double y0 = 0;
    static double y1 = 0;
    static double y2 = 0;

    //50Hz notch filter
    y0 = b0 * sample + b1 * x1 + b2 * x2 - a1 * y1 -a2 * y2;
    x2 = x1;
    x1 = sample;
    y2 = y1;
    y1 = y0;
    //
    return y0;
}

void* updateFunc(void*)
{
    FILE *rfcomm_file;
    char str[60];

    rfcomm_file = fopen ("/dev/rfcomm0","r");//rfcomm_file
    if(rfcomm_file == NULL) {
      perror(rfcomm_open_error);
      return NULL;
    }

    FILE *store_file;
    if(filename != NULL)
    {
        //printf("%s", filename);
        store_file = fopen (filename,"w");
        if(rfcomm_file == NULL) {
            perror(filename_open_error);
            return NULL;
        }
    }

    while(1){
        char c;
        int i = 0;
        while ((c = fgetc(rfcomm_file)) != '\n'){
            if(c == EOF) continue;
            str[i] = c;
            i++;
        }

        fgetc(rfcomm_file);//Wipe new line
        str[i] = '\0';
        int sample = atoi(str);
        
        /*
        //ESP32
        int sample = 0;
        sample |= (fgetc(rfcomm_file) << 8);
        sample |= fgetc(rfcomm_file);
        */        
        double y0;
        switch(notch)
        {
            case 0:
                y0 = sample;
                break;
            case 1:
                y0 = IIRnotchFilter(sample);
                break;
            case 2:
                y0 = FIRnotchFilter(sample);
                break;  
        }

        insertSample(-0.5 + y0/4096.0);
        if(filename != NULL)
        {
            fprintf(store_file, "%s,", str);//Store data as .csv file
        }
        //FSM
        if(SEGMENT_DATA)
        {
            static int everyOther = 0;
            everyOther++;
            if(everyOther == 2)
            {
                everyOther = 0;
                hearthbeatExtractionFSM(y0);
            }
        }
        //maybe wait until a buffer is full and then print it
        //printf("%d\n",sample);
    }
    //Code never comes to this place... needs fixing!!!
    fclose(rfcomm_file);
    if(filename != NULL)
    {
        fclose(store_file);
    }
}

void ECG_acquisition(void)
{
    pthread_t t;
    pthread_create(&t,NULL,updateFunc,NULL);
}