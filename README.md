# Prenos i obrada EKG signala

Ovo je projekat radjen 2020. godine, za predmet Strucna praksa i za projekat iz predmeta Multimedijalni signali i sistemi na elektrotehnickom fakultetu u Banjoj Luci.

Projekat podrazumijeva prikupljanje podataka sa nosivih EKG senzora, njegov bezicni prenos na obradnu jedinicu i na kraju njegovu obradu.

U predanom fajlu se nalazi 5 foldera:
- ClassifierTrainers
- ECG_acqusition_classification_v2
- Izvjestaji_prezentacija
- notebook_klasifikatori
- RealTimeClassifiers

Folder IzvjestajiPrezentacija sadrži tri podfoldera:
- Izvjestaj_mms u kome se nalazi izvjestaj u dva formata .pdf i .docx (izvještaj je nešto duži od ograničenja od 15 stranica, razlog toga je veliki broj slika u njemu)
- Prezentacija_mms u kome se nalazi prezentacija za odbranu projekta
- Rezultati_klasifikatora posjeduje tabele koje prikazuju rezultate postignute sa različitim klasifikatorima (iste tabele su uključene u izvještaj)

Folder notebook_klasifikatori posjeduje vise jupyter notebook fajlova koji su korišteni tokom razvoja projekta.
- Analiza dataseta.ipynb
- Algoritam_za_izdvajanje_QRS_kompleksa.ipynb
- KonvolucionaMreža_MIT-BIH.ipynb
- KonvolucionaMreža_PTB-DB.ipynb
- PotpunoPovezanaMreža_MIT-BIH.ipynb
- PotpunoPovezanaMreža_PTB-DB.ipynb
- PrenosUčenja_MIT-BIH_PTB-DB.ipynb
- SVM_klasifikator.ipynb
- STFT_klasifikator.ipynb
U folder notebook_klasifikator je potrebno postaviti dataset mitbih_test.csv, mitbih_train.csv, ptbdb_abnormal.csv, ptbdb_normal.csv da bi se notebook-ovi mogli izvršti. Dataset se može preuzeti sa: https://www.kaggle.com/shayanfazeli/heartbeat

Folder ClassifierTrainers posjeduje izvorni kod (Python skripte) za treniranje različitih klasifikatora. Po izvršenom treniranju modeli (njegova struktura i težine) se čuvaju u .h5 i .json formatu u folderu nn_structure_and_weights. Posebno, za učitavnanje modela u CPP modele je potebno sačuvati na malo drugačiji način i oni se nalaze u podfolderu CPP_compatible. Kod u CPP_compatible je isti kao i u drugim folderima osim načina na koji se model čuva na disku.
U folderu ClassifierTrainers potrebno je kopirati dataset.

Folder RealTimeClassifiers posjeduje izvorni kod (Python skripte) za klasifikacju EKG zapisa koji se prosljećuje kao ulaz u programe. Za real-time klasifikaciju potrebno je pajpovati izlaz sa ECG_acqusition programa.
sudo ./ECG_acquisition/ECG_acq -P -N -V -F2 -I | python RealTimeClassifiers/SVMclassificationMITBIH/real_time_classification.py

Folder ECG_acqusition_classification_v2 posjeduje izvorni kod pisan u C++ programskom jeziku koji pribavlja EKG signal sa hardvera i koji ga istovremeno klasifikuje.
CPP fajlovi od interesa u ovom folderu su:
- ECG_prediciton koji je zadužen za sve stvar oko učitavanja modela i vršenja predikcije
- ECG_separation koji je zadužen za izdvajanje EKG signala i njegovu normalizaciju
Program se pokreće sa: sudo ./ECG_acqusition_classification_v2/ECG_acq -P -N -V -F2 -B -6
Za ovaj program napravljen UI koji se podešava iz komandne linije. Za detaljne opcije pokrenuti program sa -H flagom.
Program izbacuje potrebu za folderom RealTimeClassifiers, ali pošto frugallyDeep biblioteka nema mogućnost učitavanja SVM modela ovaj folder je ostavljen (u tom folderu se nalazi i SVM klasifikator).

Program radi na Linux Operativnom sistemu. Prije pokretanja programa potrebno je ostvariti vezu sa hardverom za akviziciju EKG signala, a to se može uraditi sa:
sudo rfcomm connect 0 ----MAC adresa ECG_SOURCE uređaja----
(MAC adresa je: 00:00:12:09:36:43)

## Kratak opis

### Mikorkontrolerska strana
Analogni front-end cini e-Health senzorska platforma sa tri EKG elektorde. Ta platforma filtrira i pojcava EKG signal i na svojim izlazima daje njegov analogni elektricni ekvivalent u opsegu od 0V do 5V. Taj signal se dovodi na AD konvertor TM4C123GH6PM mikrokontrolea. Posto ovaj mikrokontroler radi na 3.3V, potrebno je skalirati ulazni analogni signal na opseg od 0 do 3.3V. Ovo je uradjeno jednostavnim naponskim razdjeljnikom. Rezolucija AD konvertora je 12bita, a frekvencija odmjeravanja se moze mijenjati u softveru. Nakon odmjeravanja signala, on se serijskim portom salje na HC-06 Bluetooth modul (baud rate je 115200). Kod koji se izvrsava na mikrokontroleru se nalazi u  ECG_Bluetooth_trasfer folderu.

Koristeni hardver je prikazan na slici ispod:
![setup](imgs/setup_uredjeno_oznaceno.jpg)

### Obrada signala

#### Povezivanje
Prvi korak je prijem signala sa Bluetooth modula. HC-06 modul koristi RFcomm protokol (emulacija serijskog protokola). Na Linux baziranim racunarskim sistemima, prijem podataka se jednostavno podesava, i koraci su sljedeci:
1) Prvo je potrebno upariti uredjaj sa racunarom
2) Radi povezivanja potrebno je znati MAC adresu modula. MAC adresa se moze vidjeti izvrsavanjem komande hcitool scan
3) Povezivanje se obavlja naredbom sudo rfcomm connect 0 '<'MAC adresa bluetooh uredjaja'>'<scansdptool add --channel=1 SP>

#### Vizuelizacija
Po ostvarenoj vezi, rfcomm0 fajl se pojavljuje u /dev datoteci. Citanje ovog fajla daje poslane podatke sa mikrokontolera, a upisivanje u taj fajl salje podatke na mikrokontorler.
Radi provjere ispravnosti rada, najlakse je uraditi vizuelizaciju primljenih podataka pa je u skladu sa tim napisan program u processing programskom jeziku za vizuelizaciju signala. Taj kod se moze pogledati na ecg_graph_bluetooh
U navedenom folderu se nalaze dvije verzije programa, od toga druga je novija u kojoj su neke stvari poboljsane.

Na slici ispod se moze vidjeti EKG signal dobijen pomocu osciloskopa, direktno sa EKG senzora:
![ecg_scope](imgs/ecg_oscilloscope.jpg)
Na slici ispod se moze vidjeti EKG signal dobijen preko bluetooth-a koristeci program napisan u processingu:
![ecg_bluetooth](imgs/ecg_bluetooth.jpg)

#### Vizuelizacija i klasifikacija
##### Prilagodjenje podataka
Zbog efikasnosti, ovaj dio je odradjen u C++ programskom jeziku. Vizuelizacija je ponovo uradjena koristenjem OpenGL API-ja. Prilagodjenje podataka podrazumijeva prijem podataka, tj. EKG signala i podijelu dobijenog signala na pojedinacne otkucaje. Algoritam za izdvajanje otkucaja je prikazan dijagramom koji opisuje masinu stanja.

Program se nalazi u ECG_receive folderu pod istim nazivom.
Kompilacija se izvrsava g++ kompajlerom i to g++ ECG_receive.cpp -lglut -lGL -lGLU -lpthread -o ECG_receive

![diagram](imgs/state_diagram.png)

Da bi algoritam ispravno radio potrebno je podesiti NO_RESET, DO_RESET, BUFFER_SIZE i DATASET_LENGTH parametre (oni su eksperimentalno utvrdjeni)
Algoritam izdvaja otkucaj u vremenskom domenu.

Firmware na mikrokontroleru je podesen da ima frekvenciju odmjeravanja od 500Hz i da bi se podaci prilagodili koristenom datasetu za klasifikaciju potrebno je uraditi decimaciju signala, tako da on bude odmjeren frekvencijom od 75Hz (i ako u dokumentaciji pise da je dataset odmjeren frekvencijom 125Hz koristeno je 75Hz).

Nakon sto su primljeni podaci propusteni kroz masinu stanja, oni se salju na standardni izlaz. Standardni izlaz se pajpuje (eng. pipe) sa drugim programom, napisanim u Python programskom jeziku gdje postoje odgovarajuce funkcionalnosti koje ce uraditi decimaciju. Nakon decimacije ucitava se model istrenirane neuralne mreze i njene tezine koristeci Keras i Tensorflow API. Za svaki dobijeni otkucaj srca vrsi se njegova klasifikacija i procjenjena klasa se ispisuje u konzoli. 
Kiristeni dataset se moze preuzeti sa ovog linka https://www.kaggle.com/shayanfazeli/heartbeat. Tu su ustvari dostupna dva dataseta MITBIH_arythmia dataset i ptbdb dataset.
Klasifikacija EKG signala se desava real-time.

Klasifikator se nalazi u folderu train_classifier pod nazivom real_time_classification.py

#### Treniranje klasifikatora
Za pocetak je napravljena veoma jednostava mreza sa dva Dense sloja, prvi sloj sa 50 neurona, a drugi sa 30. Dataset je ucitan i podijeljen na trening skup i validacion skup, u odnosu 0.77:0.33. Ovakva jednostavna struktura je na validacionom skupu dala tacnost od 97%. Koristenjem konvolucionih neuralnih mreza mogu se postici mnogo bolji rezultati, a to je sledeci korak u razvoju projekta.
Ova skripta se izvrsava pokretanjem iz "python train_classifier.py" unutar train_classifier foldera.

#### Pokretanje citavog sistema
Da bi se sve vidjelo na djelu, potrebno je pokretnuti oba programa i pajpovati izlaz ECG_receive programa na ulaz real_time_classification.py program.
To se radi izvrsavanjem sledece naredbe u konzol:
sudo ./ECG_receive/ECG_receive | python train_classifier/real_time_classifier.py

Rad sistema u realnom vremenu je prikazan na par slika ispod. Potrebno je imati u vidu da nakon decimacije, sumovi praticno bivaju uklonjeni. Duzi signal, iznad, je signal dobijen sa senzora, a signal ispod je izdvojen trenutni otkucaj.

Slike projektovanog hardvera koji podrzava rad sistema:

![HW1](imgs/HW1_verzija1.png)

Slike rada sistema:

![opis_prozora](imgs/Slike_rada/MITBIH1.png)
![realtime1](imgs/Slike_rada/MITBIH2.png)
![realtime2](imgs/Slike_rada/MITBIH3.png)
![realtime3](imgs/Slike_rada/PTBDB1.png)
![realtime4](imgs/Slike_rada/PTBDB1.png)
![realtime5](imgs/Slike_rada/setnja1.png)

Human activity:
https://www.kaggle.com/uciml/human-activity-recognition-with-smartphones




