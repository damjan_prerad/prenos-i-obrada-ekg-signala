
import numpy as np
import pandas as pd
import random
import matplotlib.pyplot as plt
import matplotlib.tri as mtri

from keras.utils import to_categorical

from keras.models import Sequential
from keras.layers import Dense, Activation, BatchNormalization, Dropout
from keras.optimizers import Adam

from keras.models import model_from_json

from numpy import cos, sin, pi, absolute, arange
from scipy.signal import kaiserord, lfilter, firwin, freqz, decimate

from sklearn import svm

import pickle

import sys

def main():
    #i = 0

    json_file = open("ClassifierTrainers/SVMclassiferMITBIH/nn_structure_and_weights/model_representation.json", 'r')
    loaded_model_json = json_file.read()
    json_file.close()
    loaded_model = model_from_json(loaded_model_json)
 
    loaded_model.load_weights("ClassifierTrainers/SVMclassiferMITBIH/nn_structure_and_weights/weights_representation.h5")
    print("Loaded model from disk")

    with open('ClassifierTrainers/SVMclassiferMITBIH/nn_structure_and_weights/support_vectors.pkl', 'rb') as fid:
        clf = pickle.load(fid)

    beatCounter = 0

    LAST_N = 6#Number of predictions to take into consideration
    countPredictions = np.zeros((5))
    count = 0

    while True:
        for line in sys.stdin:
            if 'Exit' == line.rstrip():
                break
            
            data = [float(i) for i in line.split()][:186]
            data = np.array([data])

            data = data.reshape(1, 186, 1)



            prediction = loaded_model.predict(data)
            #prediction = prediction.squeeze()

            final_prediciton = clf.predict(prediction)

            final_prediciton = np.where(final_prediciton < 0.5, 0, 1)
            decode = np.argmax(final_prediciton, axis=1)[0]
            #decode = 0
            #print("Predikcija : %d ------" % decode)
            #print(rectified_prediction)
            if(decode == 0):
                print("Beat no. %d -> Normal beat %d" % (beatCounter, decode))
            if(decode == 1):
                print("Beat no. %d -> Supraventricular ectopic beat %d" % (beatCounter, decode))
            if(decode == 2):
                print("Beat no. %d -> Ventricular ectopic beat %d" % (beatCounter, decode))
            if(decode == 3):
                print("Beat no. %d -> Fusion beat %d" % (beatCounter, decode))
            if(decode == 4):
                print("Beat no. %d -> Unknown beat %d" % (beatCounter, decode))

            beatCounter += 1

            countPredictions[decode] += 1
            count += 1
            if(count >= LAST_N):
                print('-------------------------------> Vecina prethodnih otkucaja su: %d' % np.argmax(countPredictions))
                countPredictions = np.zeros((5))
                count = 0
        
if __name__ == "__main__":
    main()






