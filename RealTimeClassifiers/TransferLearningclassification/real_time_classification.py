
import numpy as np
import pandas as pd
import random
import matplotlib.pyplot as plt
import matplotlib.tri as mtri

from keras.utils import to_categorical

from keras.models import Sequential
from keras.layers import Dense, Activation, BatchNormalization, Dropout
from keras.optimizers import Adam

from keras.models import model_from_json

from numpy import cos, sin, pi, absolute, arange
from scipy.signal import kaiserord, lfilter, firwin, freqz, decimate

import sys

def main():
    #i = 0

    json_file = open("ClassifierTrainers/TransferLearningMITBIHPTBDB/nn_structure_and_weights/model.json", 'r')
    loaded_model_json = json_file.read()
    json_file.close()
    loaded_model = model_from_json(loaded_model_json)
 
    loaded_model.load_weights("ClassifierTrainers/TransferLearningMITBIHPTBDB/nn_structure_and_weights/weights.h5")
    print("Loaded model from disk")

    beatCounter = 0

    LAST_N = 6#Number of predictions to take into consideration
    countPredictions = np.zeros((5))
    count = 0

    while True:
        for line in sys.stdin:
            if 'Exit' == line.rstrip():
                break
            
            data = [float(i) for i in line.split()][:186]
            data = np.array([data])

            data = data.reshape(1, 186, 1)
            
            
            
            #downsample by 4

            #print(len(data))
            #decimated = decimate(x = data, q = 4, n = 200, ftype = 'fir')
            #print(len(decimated))
            #print('done')
            
            #raw = np.zeros(186)
            #raw[:len(decimated)] = decimated
                        
            #----------------------------
            #fig, axs = plt.subplots(2)
            #x = np.linspace(0, 186, 186)
            #xo = np.linspace(0, 744, 744)
            #xoo = np.linspace(0, 186, 186)
            #axs[0].plot(xo, data)
            #axs[1].plot(x, decimated)
            #axs[2].plot(xoo, raw)
            #plt.show()
            #----------------------------

            prediction = loaded_model.predict(data)
            rectified_prediction = np.where(prediction < np.max(prediction), 0, 1)
            decode = np.argmax(rectified_prediction, axis=1)[0]
            #decode = 0
            print("Predikcija : %d ------" % decode)
            #print(rectified_prediction)
            if(decode == 0):
                print("Beat no. %d -> Normal beat %d" % (beatCounter, decode))
            if(decode == 1):
                print("Beat no. %d -> Abnormal beat %d" % (beatCounter, decode))

            beatCounter += 1

            countPredictions[decode] += 1
            count += 1
            if(count >= LAST_N):
                print('-------------------------------> Vecina prethodnih otkucaja su: %d' % np.argmax(countPredictions))
                countPredictions = np.zeros((5))
                count = 0

            #print(data)
            #sys.stdout.flush()
            #print("Primljeno:\n %d" % i)
            #print(line)
            #i += 1 
        #sys.stdin.flush()
        
if __name__ == "__main__":
    main()






