/*
 * UART_specific.h
 *
 *  Created on: Dec 31, 2019
 *      Author: damja
 */

#ifndef UART_SPECIFIC_H_
#define UART_SPECIFIC_H_

void ConfigureUART(void);
void printValues(float AccX, float AccY, float AccZ, float GyroX, float GyroY, float GyroZ);

void ConfigureUART(void)
{
    ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOB);

    ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_UART1);

    ROM_GPIOPinConfigure(GPIO_PB0_U1RX);
    ROM_GPIOPinConfigure(GPIO_PB1_U1TX);
    ROM_GPIOPinTypeUART(GPIO_PORTB_BASE, GPIO_PIN_0 | GPIO_PIN_1);

    UARTClockSourceSet(UART1_BASE, UART_CLOCK_PIOSC);

    UARTStdioConfig(1, 115200, 16000000);
}

#endif /* UART_SPECIFIC_H_ */
