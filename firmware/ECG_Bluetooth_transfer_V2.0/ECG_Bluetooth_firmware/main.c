

/**
 * main.c
 *
 * Author: Damjan Prerad
 * Ovaj kod je napravljen u svrhe predmetna strucna praksa na elektrotehnckom fakultetu u Banja Luci
 *
 * Radi se odmjeravanje EKG signala i prosljedjivanje na HC-06 Bluetooth modul
 */
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>

#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "driverlib/gpio.h"
#include "driverlib/pin_map.h"
#include "driverlib/sysctl.h"
#include "driverlib/uart.h"

#include "utils/uartstdio.h"
#include "driverlib/rom.h"

#include <stdarg.h>
#include "inc/hw_i2c.h"
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "driverlib/i2c.h"
#include "driverlib/sysctl.h"
#include "driverlib/gpio.h"
#include "driverlib/pin_map.h"
#include "driverlib/adc.h"

#include "inc/hw_ints.h"
#include "driverlib/interrupt.h"

#include "driverlib/timer.h"

#include "UART_specific.h"

#include <stdint.h>

#define SAMPLE_RATE_SELECTOR1    (GPIOPinRead(GPIO_PORTF_BASE, GPIO_PIN_3))
#define SAMPLE_RATE_SELECTOR2    (GPIOPinRead(GPIO_PORTF_BASE, GPIO_PIN_4))

uint8_t go_flag = 0;

uint32_t divisor = 1;
uint32_t counter = 0;

uint32_t state1 = 0;
uint32_t state2 = 0;

uint8_t a = 0;
uint8_t b = 0;

uint32_t toggle = 0;

void Timer0Isr(void)
{

  ROM_TimerDisable(TIMER0_BASE, TIMER_A);
  ROM_TimerIntClear(TIMER0_BASE, TIMER_TIMA_TIMEOUT);  // Clear the timer interrupt

  //Maybe do this in GPIO interrupt
  //Set divisor
  divisor = 10000000;

  a = GPIOPinRead(GPIO_PORTF_BASE, GPIO_PIN_4) & GPIO_PIN_4;
  b = GPIOPinRead(GPIO_PORTF_BASE, GPIO_PIN_3) & GPIO_PIN_3;

  if(a == 0)
  {
      state1 = 0xffffffff;
  }
  else state1 = 0;

  if(b == 0)
  {
      state2 = 0xffffffff;
  }
  else state2 = 0;

  if( a == 0 && b == 0) {divisor = 1;ROM_TimerLoadSet(TIMER0_BASE, TIMER_A, (40000000 / 2) / 125);}
  if( a != 0 && b == 0) {divisor = 2;ROM_TimerLoadSet(TIMER0_BASE, TIMER_A, (40000000 / 2) / 250);}
  if( a == 0 && b != 0) {divisor = 4;ROM_TimerLoadSet(TIMER0_BASE, TIMER_A, (40000000 / 2) / 500);}
  if( a != 0 && b != 0) {divisor = 8;ROM_TimerLoadSet(TIMER0_BASE, TIMER_A, (40000000 / 2) / 1000);}

  GPIOPinWrite(GPIO_PORTF_BASE, GPIO_PIN_1, state1);
  GPIOPinWrite(GPIO_PORTF_BASE, GPIO_PIN_2, state2);
  ROM_TimerEnable(TIMER0_BASE, TIMER_A);
  //-----------
  //counter++;
  //if(counter >= divisor)
  //{
  //    counter = 0;
  //    go_flag = 1;
  //}
}

int main(void)
{
    ROM_SysCtlClockSet(SYSCTL_SYSDIV_5 | SYSCTL_USE_PLL | SYSCTL_XTAL_16MHZ | SYSCTL_OSC_MAIN);//Clock is 40MHz : USE_PLL -> 200MHz, SYSDIV -> 5 => 200MHz / 5 => 40MHz
    ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOB);
    ConfigureUART();

    uint32_t ADCValues[1];

    //Sample rate selector pins

    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOB);
    GPIOPinTypeGPIOOutput(GPIO_PORTB_BASE, GPIO_PIN_6);

    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);
    GPIOPinTypeGPIOOutput(GPIO_PORTF_BASE, GPIO_PIN_1|GPIO_PIN_2);

    GPIOPinTypeGPIOInput(GPIO_PORTF_BASE, GPIO_PIN_3|GPIO_PIN_4);
    GPIOPadConfigSet(GPIO_PORTF_BASE, GPIO_PIN_3|GPIO_PIN_4, GPIO_STRENGTH_8MA, GPIO_PIN_TYPE_STD_WPU);
    //------------------------

    //Timer setup
    ROM_SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER0);
    ROM_TimerConfigure(TIMER0_BASE, TIMER_CFG_PERIODIC);   // 32 bits Timer
    TimerIntRegister(TIMER0_BASE, TIMER_A, Timer0Isr);    // Registering  isr
    ROM_TimerEnable(TIMER0_BASE, TIMER_A);
    ROM_IntEnable(INT_TIMER0A);
    ROM_TimerIntEnable(TIMER0_BASE, TIMER_TIMA_TIMEOUT);

    //(master_clock/2) / wanted_clock => 400MHz / 500Hz = 80000
    ROM_TimerLoadSet(TIMER0_BASE, TIMER_A, (40000000 / 2) / 500);
    //---------------

    SysCtlPeripheralEnable(SYSCTL_PERIPH_ADC0);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOE);
    GPIOPinTypeADC(GPIO_PORTE_BASE, GPIO_PIN_3);
    ADCSequenceConfigure(ADC0_BASE, 3, ADC_TRIGGER_PROCESSOR, 0);
    ADCSequenceStepConfigure(ADC0_BASE, 3, 0, ADC_CTL_CH0 | ADC_CTL_IE |
                                 ADC_CTL_END);
    ADCSequenceEnable(ADC0_BASE, 3);
    ADCIntClear(ADC0_BASE, 3);

    while(1)
    {
        //if(go_flag == 1)
        //{
            toggle = ~toggle;
            //GPIOPinWrite(GPIO_PORTB_BASE, GPIO_PIN_6, toggle);

            ADCProcessorTrigger(ADC0_BASE, 3);
            while(!ADCIntStatus(ADC0_BASE, 3, false))
                    {
                    }
            ADCIntClear(ADC0_BASE, 3);

            ADCSequenceDataGet(ADC0_BASE, 3, ADCValues);
            UARTprintf("%d\n", ADCValues[0]);
            //go_flag = 0;
        //}
        SysCtlSleep();
        //SysCtlDelay(4 * (SysCtlClockGet() / 3 / 1000));
    }

}
