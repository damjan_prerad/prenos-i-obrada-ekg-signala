Ovdje se nalazi firmware koji radi digitalizaciju EKG signala i prosljeduje ga Bluetooth modulu.
Razvojno okruzenje je CodeComposser.
Mikrokontroler: TM4C123GH6PM
(Tiva C series TM4C123G LaunchPad Evaluation Kit)

- ECG_Bluetooth_transfer -> prva verzija
- ECG_Bluetooth_transfer_V2.0 -> koristi interrupt, dodana je mogucnost izbora frekvencije odmjeravanja, uklonjeno par bugova, smanjena duzina UART poruke, vizuelni prikaz na LE diodama frekvencije odmjeravanja