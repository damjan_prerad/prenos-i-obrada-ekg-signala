
#Ova skripta izvrsava zadatak prihvatanja EKG signala dobijenog preko Bluetootha pomocu rfcomm protokola
#i cuvanja istog u fajl.

import _thread
import time

def input_thread(a_list):
    input()
    a_list.append(True)

def save_ecg(sink):
    a_list = []
    _thread.start_new_thread(input_thread, (a_list,))
    while not a_list:
        print('Looping until ENTER pressed')
        sink.write('Hello\n')
        time.sleep(1)
    


print("Cuvanje EKG signala u fajl")
#source = open('rfcomm0','w') 
sink = open('ecg.data', 'w')
save_ecg(sink)
sink.close()
print('Kraj... Izlazak iz programa!')