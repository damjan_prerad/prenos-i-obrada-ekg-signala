import processing.serial.*;

Serial dataInput;

int[] ECGdata;


void setup(){
  dataInput = new Serial(this, Serial.list()[0], 115200);
  for(int i = 0; i < Serial.list().length; i++)
    println(Serial.list()[i]);
  println("\007");
  size(900, 900);
  ECGdata = new int [width];
}

int ecg;

void updateECGDrawing(int ecg){
  for(int i = ECGdata.length - 1; i > 0; i--){
    ECGdata[i] = ECGdata[i - 1];
  }
  ECGdata[0] = ecg / 10;
  stroke(0,0,255);
  for(int i = 1; i < ECGdata.length; i++){
    line(ECGdata.length - i + 1, ECGdata[i - 1] + height / 2, ECGdata.length - i, ECGdata[i] + height / 2 );  
  }
}

void draw(){
  while (dataInput.available() < 5);/// {
    String inBuffer = dataInput.readString();   
    //if (inBuffer != null) {
      String singleData [] = split(inBuffer, ' ');
      if(singleData.length == 2){
        //intln(split(singleData[1], '\n')[0]);
        println(singleData[1]);
        String usefulData = trim(split(singleData[1], '\n')[0]);
        if(!usefulData.equals("")){
          ecg = -Integer.valueOf(usefulData);
          //println("\007");
          if(-ecg > 2450){
            //println("\007");
          }
          //println(ecg);
    
          background(204);
          updateECGDrawing(ecg);
        }
      //}
    //}
    dataInput.clear();
  }
}
