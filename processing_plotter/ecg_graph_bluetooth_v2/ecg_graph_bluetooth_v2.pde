import processing.serial.*;

Serial dataInput;

int[] ECGdata;
BufferedReader ecg_rec;
String ecg_line;

void setup(){
  ecg_rec = createReader("/dev/rfcomm0"); 
  size(900, 900);
  ECGdata = new int [width];
}

int ecg;

void updateECGDrawing(int ecg){
  for(int i = ECGdata.length - 1; i > 0; i--){
    ECGdata[i] = ECGdata[i - 1];
  }
  ECGdata[0] = ecg / 15;
  stroke(0,0,255);
  for(int i = 1; i < ECGdata.length; i++){
    line(ECGdata.length - i + 1, ECGdata[i - 1] + height / 2, ECGdata.length - i, ECGdata[i] + height / 2 );  
  }
}

void draw(){
  
  try {
    ecg_line = ecg_rec.readLine();
  } catch (IOException e) {
    e.printStackTrace();
    ecg_line = null;
  }
  if (ecg_line == null) {
    noLoop();  
  } else {
    //println(ecg_line);
    String raw_data = ecg_line.replaceAll("[^\\d.-]", "");
    println(raw_data);
    if(!raw_data.equals("")){
      int ecg = Integer.valueOf(raw_data);
      background(204);
      updateECGDrawing(-ecg);
    }
  }
}
